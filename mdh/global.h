/*
   This file contains global variables. 
   Ideally, it should be empty, and all data contained in program structures.
   
*/
#define DEBUG(var) if (debug) fprintf(fpdebug, #var " = %g\n",(double) (var)); oflush();
#define MAXDEBUG(var) if (maxdebug) fprintf(fpdebug, #var " = %g\n",(double) (var)); oflush();
#define DEBUGS(str) if (debug) fprintf(fpdebug,str); oflush();
#define MAXDEBUGS(str) if (maxdebug) fprintf(fpdebug,str); oflush(); 
#define DEBUGSR(str,var) if (debug) fprintf(fpdebug, "%s " #var " = %g\n",str,(double) (var)); oflush();
#define MAXDEBUGSR(str,var) if (maxdebug) fprintf(fpdebug, "%s " #var " = %g\n",(str),((double) (var))); oflush();
#define DEBUGSRR(str,var,var2) if (debug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g\n",str,(double) (var),(double) (var2)); oflush();
#define MAXDEBUGSRR(str,var,var2) if (maxdebug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g\n",(str),((double) (var)),((double) (var2))); oflush();
#define DEBUGSRRR(str,var,var2,var3) if (debug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g\n",str,(double) (var),(double) (var2),(double) (var3)); oflush();
#define DEBUGSRRRR(str,var,var2,var3,var4) if (debug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g " #var4 " = %g\n",str,(double) (var),(double) (var2),(double) (var3),(double) (var4)); oflush();
#define DEBUGSRRRRR(str,var,var2,var3,var4,var5) if (debug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g " #var4 " = %g " #var5 " = %g\n",str,(double) (var),(double) (var2),(double) (var3),(double) (var4),(double) (var5)); oflush();
#define MAXDEBUGSRRR(str,var,var2,var3) if (maxdebug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g\n",str,(double) (var),(double) (var2),(double) (var3)); oflush();
#define MAXDEBUGSRRRR(str,var,var2,var3,var4) if (maxdebug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g " #var4 " = %g\n",str,(double) (var),(double) (var2),(double) (var3),(double) (var4)); oflush();
#define MAXDEBUGSRRRRR(str,var,var2,var3,var4,var5) if (maxdebug) fprintf(fpdebug, "%s " #var " = %g " #var2 " = %g " #var3 " = %g " #var4 " = %g " #var5 " = %g\n",str,(double) (var),(double) (var2),(double) (var3),(double) (var4),(double) (var5)); oflush();

typedef int logical;
#define True 1
#define False 0
typedef double real; /* This enables simple changing between single and double prec */


extern FILE *fpdebug;      /* Same as fpdb in struct file */
extern logical debug;      /* debug to file stdout */
extern logical maxdebug;   /* maximum depug to stdout */

#ifdef MAX
#undef MAX
#endif
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#ifdef MIN
#undef MIN
#endif

#define MIN(a,b) ( (a) > (b) ? (b) : (a) )
#define WITHIN(a,b,c) ( MIN(MAX((a),(b)),(c)) )
#define oabs(a) ( (a) < 0 ? -(a) : (a) ) 

#ifndef abs
#define abs(a) ( (a) < 0 ? -(a) : (a) ) 
#endif

#define sign(a) ( (a) > 0 ? 1 : (a) < 0 ? -1 : 0 )

#ifdef MAIN
#define EXTERN
#else
#define EXTERN extern
#endif

#define ELEMENTS(arr) (sizeof(arr) / sizeof(arr[0])) /* BS - Get Number of elements in array*/

#include "arraysizes.h"
#include "src/structs.h"

#define iniind(a,b,c,d) ( (a)*MAXATOM*MAXINISTATE*MAXAMORPH+(b)*MAXATOM*MAXINISTATE+(c)*MAXATOM+(d) )
/*layer-am.state-inistate-atom*/