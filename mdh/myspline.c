#include "common.h" // add this back in the end.. FIX
#include "src/myspline.h"
/*
  The algorithms for spline interpolation of rho and alpha.

*/

/*int main() // only for debugging spline functions, remove this in the end FIX
{
  char infile[120], outfile[120];
  struct _IO_FILE *file, *of;
  double alpha, beta, salpha, sbeta, x, result;
  int i,ii;
  int counts;
  double *funcx, *funcy, *y2;


  // read in a file
  printf("Enter name of file to be splined: ");
  scanf("%s", infile);

  printf("Enter point at which to find interpolation: ");
  scanf("%lf", &x);
  printf("Reading file %s and interpolating at point %0.8lf \n",infile,x);

  funcx= (double *)malloc(MAXSTOPP*sizeof(double));
  funcy=(double *)malloc(MAXSTOPP*sizeof(double));
  y2=(double *)malloc(MAXSTOPP*sizeof(double));
    for(i=0;i<MAXSTOPP;i++)
      {
  funcx[i]=0.;
  funcy[i]=0.;
  y2[i]=0.;
      }

  counts=0;
  printf("Reading file..\n");
  file=fopen(infile,"r");
  if(file==NULL)
    {
    printf("Can't read %s\n",infile);
    exit(1);
    }

  for(i=0;i<MAXSTOPP;i++)
    {
      if(!feof(file))
  {
    ii=fscanf(file,"%lf %lf",&alpha,&beta);
    funcx[i]=alpha;
    funcy[i]=beta;
    counts++;
  }
    }
  fclose(file);



  // spline interpolation
  ii=spline(funcx,funcy,counts,99e30,99e30,y2);
  if (ii!=0)
    {
      printf("Problem in spline.\n");
      exit(1);
    }
  of=fopen(outfile,"w");

  result=splint(funcx,funcy,y2,counts,x);

    printf("The interpolated point is: %lf %lf",x,result);
    return 0;
}*/

double splint(double *xa, double *ya, double *y2a, int n, double x)
{
  int k, khi, klo;
  double a, b, h, y;

  klo = 0;
  khi = n - 1;

  while (khi - klo > 1)
  {
    k = (khi + klo) / 2;
    if (xa[k] > x)
      khi = k;
    else
      klo = k;
  }

  h = xa[khi] - xa[klo];
  if (h == 0.0)
  {
    printf("Error in splint. ");
    exit(1);
  }
  a = (xa[khi] - x) / h;
  b = (x - xa[klo]) / h;
  y = a * ya[klo] + b * ya[khi] + (pow(a, 3 - a) * y2a[klo] + pow(b, 3 - b) * y2a[khi]) * pow(h, 2) / 6.0;

  return (y);
}

int spline(double *x, double *y, int n, double yp1, double ypn, double *y2)
{
  int i, k;
  double p, qn, sig, un, u[MAXSTOPP];

  if (yp1 > .99e30)
  {
    y2[0] = 0.0;
    u[0] = 0.0;
  }
  else
  {
    y2[0] = -0.5;
    u[0] = (3. / (x[1] - x[0])) * ((y[1] - y[0]) / (x[1] - x[0]) - yp1);
  }
  for (i = 1; i < n - 2; i++)
  {
    sig = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
    p = sig * y2[i - 1] + 2.0;
    y2[i] = (sig - 1.0) / p;
    u[i] = (6.0 * ((y[i + 1] - y[i]) / (x[i + 1] - x[i]) - (y[i] - y[i - 1]) / (x[i] - x[i - 1])) / (x[i + 1] - x[i - 1]) - sig * u[i - 1]) / p;
  }

  if (ypn > .99e30)
  {
    qn = 0.0;
    un = 0.0;
  }
  else
  {
    qn = 0.5;
    un = (3. / (x[n - 1] - x[n - 2])) * (ypn - (y[n - 1] - y[n - 2]) / (x[n - 1] - x[n - 2]));
  }
  y2[n - 1] = (un - qn * u[n - 2]) / (qn * y2[n - 2] + 1.0);

  for (k = n - 2; k >= 1; k--)
  {
    y2[k] = y2[k] * y2[k + 1] + u[k];
  }

  return 0;
}
/*    The fortran code:
!***********************************************************************

SUBROUTINE splint(xa,ya,y2a,n,x,y)
INTEGER :: n
REAL :: x,y,xa(n),y2a(n),ya(n)
INTEGER :: k,khi,klo
REAL :: a,b,h
klo=1
khi=n
1 if (khi-klo.gt.1) then
k=(khi+klo)/2
if(xa(k).gt.x)then
   khi=k
else
   klo=k
endif
goto 1
endif
h=xa(khi)-xa(klo)
if (h.eq.0.) then
write (*,*) "Error in spline"
return
endif
a=(xa(khi)-x)/h
b=(x-xa(klo))/h
y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
return
END SUBROUTINE splint

!***************************************************************************

SUBROUTINE spline(x,y,n,yp1,ypn,y2)
INTEGER n,NMAX
REAL yp1,ypn,x(n),y(n),y2(n)
PARAMETER (NMAX=2000)
INTEGER i,k
REAL p,qn,sig,un,u(NMAX)
if (yp1.gt..99e30) then
y2(1)=0.
u(1)=0.
else
y2(1)=-0.5
u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
endif
do i=2,n-1
sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
p=sig*y2(i-1)+2.
y2(i)=(sig-1.)/p
u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
enddo

if (ypn.gt..99e30) then
qn=0.
un=0.
else
qn=0.5
un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
endif
y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
do k=n-1,1,-1
y2(k)=y2(k)*y2(k+1)+u(k)
enddo

return
END SUBROUTINE spline

*/
