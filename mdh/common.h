
/*
  This header does NOT contain common blocks. It should contain very general
stuff that are needed in all subroutine libraries
*/
#ifndef __COMMON__
#define __COMMON__

/* default c libs */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <limits.h>

#include "fmath.h"

/* Function declarations header files */
#include "src/accelerations.h"
#include "src/borders.h"
#include "src/elstop.h"
#include "src/inistatecalc.h"
#include "src/initparams.h"
#include "src/moveslice.h"
#include "src/myspline.h"
#include "src/output.h"
#include "src/potential.h"
#include "src/random.h"
#include "src/readcoords.h"
#include "src/readin.h"
#include "src/readvar.h"
#include "src/recoil.h"
#include "src/solve.h"
#include "src/spline.h"
#include "src/start.h"
#include "src/time.h"


#include "global.h"

#endif /*__COMMON__*/
