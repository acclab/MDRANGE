#include "common.h"
#include "src/inistatecalc.h"

void inistatecalc(
struct iniat *iniat,
struct pot *pot,
struct potcrit *potcrit,
struct time *time,
struct unit *unit,
struct type *type,
struct file *file,
struct box *box,
struct physical *physical,
struct layers *layers,
int Ntypes)
{
   struct atom *at;
   struct atomex *atex;
   struct atomflag *atflag;

   struct box *box2;

   int nlayer, ninistate, nat, namorph;
   real t, T, tmax;
   int i, intcounter;
   real deltarmean;
   point *origpos;
   logical update = False;
   real sd;
   int j, k, n;

   at = (struct atom *)malloc(MAXATOM * sizeof(struct atom));
   atex = (struct atomex *)malloc(MAXATOM * sizeof(struct atomex));
   atflag = (struct atomflag *)malloc(MAXATOM * sizeof(struct atomflag));
   origpos = (point *)malloc(MAXATOM * sizeof(point));

   box2 = (struct box *)malloc(sizeof(struct box));

   gaussianvels(iniat, type, file, unit, layers, physical->Tini);
   fprintf(file->fpo, "\nStarting inistate calculations\n");
   fprintf(file->fpo, "------------------------------\n\n");

   for (nlayer = 0; nlayer < layers->Nlayers; nlayer++)
   {
      for (namorph = 0; namorph < layers->layer[nlayer].Namorph; namorph++)
      {

         for (ninistate = 0; ninistate < layers->Ninistate; ninistate++)
         {
            fprintf(file->fpo, "Layer # %d amorphstate # %d Inistate calc. # %d\n", nlayer, namorph, ninistate);

            /*
               Do necessary initializations
            */

            time->dtreal = time->Dtini * 1e-15;
            inittsteps(time, unit);
            t = 0.0;
            nat = layers->layer[nlayer].natoms[namorph];

            copyinilayer(iniat, at, atex, atflag, nlayer, ninistate, namorph, nat);

            if (file->printcoordsi)
               printcoveac(at, nat, t);

            DEBUG(T);
            box->periodic = True;
            box->warnoutside = True;
            potcrit->r0 = potcrit->R0ini;
            potcrit->rm = potcrit->Rmini;
            DEBUG(nat);
            for (i = 0; i < nat; i++)
            {
               origpos[i].x = at[i].x;
               origpos[i].y = at[i].y;
               origpos[i].z = at[i].z;
            }
            for (i = 0; i < nat; i++)
               change(at, type, i);

            /*
               Then the actual inistate calculation
            */

            box2->periodic = box->periodic; /*setting sizes for checking*/
            box2->warnoutside = box->warnoutside;
            box2->size = layers->layer[nlayer].boxsize[namorph];

            intcounter = 0;
            tmax = time->Timeini;
            if (layers->layer[nlayer].Timeinior >= 0.0)
               tmax = layers->layer[nlayer].Timeinior;
            DEBUGSRRR("Going into inistate loop", nlayer, ninistate, tmax);

            if (layers->layer[nlayer].debyet <= 0)
            {
               printf("Debye T not set, attempting inistate MD simulation\n");

               setcelltemp(at, atflag, nat, unit, 2 * physical->Tini); /* The factor 2 is not a bug */
               getcelltemp(at, atflag, nat, unit, &T, &(physical->ekin));

               if (physical->Tini > 0.0)
               {
                  for (t = 0; t <= tmax; t += time->Dtini)
                  {
                     MAXDEBUGSR("\nStart of inistate loop\n", t);
                     solve1(at, time, atflag, box, nat, Ntypes);
                     borders(at, box2, nat);
                     accelerations(at, atex, atflag, pot, potcrit, box, physical, unit, nat, 0, &update, file);
                     solve2(at, time, atflag, box, nat, Ntypes);
                     getcelltemp(at, atflag, nat, unit, &T, &(physical->ekin));
                     MAXDEBUGSRRRR("ini", t, T, physical->ekin, physical->epot);

                     intcounter++;
                     if (intcounter >= box->Iniscaleint)
                     {
                        intcounter = 0;
                        MAXDEBUGSR("Subtracting CM vel. and acceleration", t);
                        subtractcmvel(at, nat);
                        subtractcmacc(at, nat);
                     }
                     MAXDEBUG(T);

                     if (file->printcoordsi)
                        printcoveac(at, nat, t);
                  }
               }
               getmeandisplacement(&deltarmean, origpos, at, nat, box);
               fprintf(file->fpo, "Mean displacement (sim.) %g\n\n", deltarmean);
               setinilayer(iniat, at, nlayer, ninistate, namorph, nat); /*JARKKO-damage*/
            }
            else
            {
               /* 21.3 is sqrt(9 hbar^2/k_B u)
             This simple expression comes from second order approximation of an
             elliptic integral. See Gemmel, Rev. Mod. Phys. 46 (1974) 129.
               */
               MAXDEBUGSRRR("Debye displ", physical->Tini, type[1].m, layers->layer[nlayer].debyet);

               /* The debye/attr.pot switch parameter and the Debye temp have
                    been added to struct layers in param.in */
               for (i = 0; i < nat; i++)
               {
                  if (layers->layer[nlayer].debyet > 0.0)
                  {
                     /* Default model from old times */
                     sd = 21.3 * sqrt(physical->Tini / type[at[i].type].m) / (sqrt(3.0) * layers->layer[nlayer].debyet);
                     if (layers->layer[nlayer].debyemodel == 2)
                     {
                        /* Same as above except prefactor more accuratelty calculated */
                        sd = 20.89 * sqrt(physical->Tini / type[at[i].type].m) / (sqrt(3.0) * layers->layer[nlayer].debyet);
                     }
                     else if (layers->layer[nlayer].debyemodel == 1)
                     {
                        sd = debyedisplacements_stegun(physical->Tini, layers->layer[nlayer].debyet, type[at[i].type].m);
                     }
                  }
                  else
                  {
                     fprintf(stderr, "Debye T not set or negative %g although needed %d\n", layers->layer[nlayer].debyet, nlayer);
                     exit(0);
                  }
                  MAXDEBUGSRRR("Debye displ for atom", i, at[i].type, sd);

                  if(file->useThermalDisplacements)
                  {
                     at[i].x = at[i].x + sd * gaussianrand();
                     at[i].y = at[i].y + sd * gaussianrand();
                     at[i].z = at[i].z + sd * gaussianrand();
                  }
                  // fprintf(file->fpo, "%g %g %g 1 \n", at[i].x, at[i].y, at[i].z); // GPK - create coords.in from this
               }
               /* fprintf(file->fpo,"Debye model %d theoretical mean 3D displ. %g for atom type %d\n",layers->layer[nlayer].debyemodel,sd*sqrt(3),at[i-1].type); */
               getmeandisplacement(&deltarmean, origpos, at, nat, box);
               fprintf(file->fpo, "Actual mean 3D displacement (Debye) %g\n\n", deltarmean);
               setinilayer(iniat, at, nlayer, ninistate, namorph, nat);
            }

            DEBUGSR("End of inistate calcs for layer ", nlayer);
         } /*inistate looppi*/

         DEBUGS("End of inistate calculations for all layers\n");
      } /* am-looppi*/
      DEBUGSR("End of inistate calcs for am state ", namorph);
   } /*nlayer looppi*/
}

void gaussianvels(
struct iniat *iniat,
struct type type[MAXTYPE],
struct file *file,
struct unit *unit,
struct layers *layers,
real T)
{
   int nlayer, ninistate, n, namorph;
   real vx, vy, vz, vxsum = 0.0, vysum = 0.0, vzsum = 0.0, v, vtot;
   real unirand(), gaussianrand();

   DEBUGS("Giving gaussian initial velocities to atoms\n");

   for (nlayer = 0; nlayer < layers->Nlayers; nlayer++)
   {
      DEBUGSRR("Copying coordinates from layer to inistate arrays", nlayer, layers->Ninistate);
      for (namorph = 0; namorph < layers->layer[nlayer].Namorph; namorph++)
      {
         if (debug)
            printf("gaussianvel :layer %d, amstate %d\n", nlayer, namorph);
         for (ninistate = 1; ninistate < layers->Ninistate; ninistate++)
         {
            for (n = 0; n < layers->layer[nlayer].natoms[namorph]; n++)
            {
               /*JARKKO-damage*/
               iniat[iniind(nlayer, namorph, ninistate, n)].x = iniat[iniind(nlayer, namorph, 0, n)].x;
               iniat[iniind(nlayer, namorph, ninistate, n)].y = iniat[iniind(nlayer, namorph, 0, n)].y;
               iniat[iniind(nlayer, namorph, ninistate, n)].z = iniat[iniind(nlayer, namorph, 0, n)].z;
               iniat[iniind(nlayer, namorph, ninistate, n)].type = iniat[iniind(nlayer, namorph, 0, n)].type;
               /*printf("%g,%g,%g\n",iniat[iniind(nlayer,namorph,ninistate,n)].x,iniat[iniind(nlayer,namorph,ninistate,n)].y,iniat[iniind(nlayer,namorph,ninistate,n)].z);*/
            } /*atom*/
         } /*inistate*/
      } /*amorp*/
   } /* layer*/

   v = sqrt(2 * T / unit->tempconv);
   if (debug)
      printf("Initial rms velocity %g, in m/s %g, in K %g\n",
             v, v * unit->vel[0], T);
   for (nlayer = 0; nlayer < layers->Nlayers; nlayer++)
   {
      MAXDEBUGSR("Number of layer ", nlayer);
      for (namorph = 0; namorph < layers->layer[nlayer].Namorph; namorph++)
      {
         for (ninistate = 0; ninistate < layers->Ninistate; ninistate++)
         {
            MAXDEBUGSR("Number of inistate calc. in layer", ninistate);
            vtot = 0;
            vxsum = vysum = vzsum = 0.0; /*JARKKO-fix*/
            for (n = 0; n < layers->layer[nlayer].natoms[namorph]; n++)
            {
               vx = v * gaussianrand();
               vy = v * gaussianrand();
               vz = v * gaussianrand();
               /*JARKKO-damage*/
               iniat[iniind(nlayer, namorph, ninistate, n)].vx = vx;
               vxsum += vx;
               iniat[iniind(nlayer, namorph, ninistate, n)].vy = vy;
               vysum += vy;
               iniat[iniind(nlayer, namorph, ninistate, n)].vz = vz;
               vzsum += vz;
               vtot += sqrtf(vx * vx + vy * vy + vz * vz);
            }
            vtot /= layers->layer[nlayer].natoms[namorph];
            DEBUG(vtot);
            vxsum /= layers->layer[nlayer].natoms[namorph];
            vysum /= layers->layer[nlayer].natoms[namorph];
            vzsum /= layers->layer[nlayer].natoms[namorph];
            if (debug)
               printf("vsums,initsate,amstate,layer : %g %g %g %d %d %d\n", vxsum, vysum, vzsum, ninistate, namorph, nlayer);

            
            for (n = 0; n < layers->layer[nlayer].natoms[namorph]; n++)
            {
               /*JARKKO-damage*/
               iniat[iniind(nlayer, namorph, ninistate, n)].vx -= vxsum;
               iniat[iniind(nlayer, namorph, ninistate, n)].vy -= vysum;
               iniat[iniind(nlayer, namorph, ninistate, n)].vz -= vzsum;
            }
         
            MAXDEBUG(vxsum);
            MAXDEBUG(vysum);
            MAXDEBUG(vzsum);
         } /*initstate-looppi*/
      } /*amorph-looppi*/
   } /*layer-looppi*/
}

void copyinilayer(
struct iniat *iniat,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
int nlayer,
int ninistate,
int namorph,
int nat)
{
   int i;

   for (i = 0; i < nat; i++)
   {
      /*JARKKO-damage*/
      at[i].x = iniat[iniind(nlayer, namorph, ninistate, i)].x;
      at[i].y = iniat[iniind(nlayer, namorph, ninistate, i)].y;
      at[i].z = iniat[iniind(nlayer, namorph, ninistate, i)].z;
      at[i].vx = iniat[iniind(nlayer, namorph, ninistate, i)].vx;
      at[i].vy = iniat[iniind(nlayer, namorph, ninistate, i)].vy;
      at[i].vz = iniat[iniind(nlayer, namorph, ninistate, i)].vz;
      at[i].ax = at[i].ay = at[i].az = 0.0;
      at[i].a1x = at[i].a1y = at[i].a1z = 0.0;
      at[i].a2x = at[i].a2y = at[i].a2z = 0.0;
      at[i].type = iniat[iniind(nlayer, namorph, ninistate, i)].type;

      atex[i].pot = 0.0;
      atflag[i].moving = True;
      atflag[i].interact = True;
      atflag[i].recoil = False;
      atflag[i].energetic = False;
   }
}

void setinilayer(
struct iniat *iniat,
struct atom *at,
int nlayer,
int ninistate,
int namorph,
int nat)
{
   int i;

   for (i = 0; i < nat; i++)
   {
      /*JARKKO-damage*/
      iniat[iniind(nlayer, namorph, ninistate, i)].x = at[i].x;
      iniat[iniind(nlayer, namorph, ninistate, i)].y = at[i].y;
      iniat[iniind(nlayer, namorph, ninistate, i)].z = at[i].z;
      iniat[iniind(nlayer, namorph, ninistate, i)].vx = at[i].vx;
      iniat[iniind(nlayer, namorph, ninistate, i)].vy = at[i].vy;
      iniat[iniind(nlayer, namorph, ninistate, i)].vz = at[i].vz;
      iniat[iniind(nlayer, namorph, ninistate, i)].type = at[i].type;
   }
}

/*
   Inputs:
   T = material temperature in K
   TDebye = Debye temperature in K,
   m=Atom mass in amu,

   The rms thermal displacement amplitude is evaluated using the Debye
   model.  See M. Blackman in Handbuch der Physik, Volume VII, Part 1
   (Springer-Verlag, Berlin, 1955), page 377.  The approximation to the
   Debye function is from I. A. Stegun in Handbook of Mathematical Fun-
   ctions (National Bureau of Standards, Washington, 1964), page 998.


*/
real debyedisplacements_stegun(real T, real TDebye,real m)
{
  real W;
  real cvamp[11], scale, tamp;
  real QX, QA, QZ, QB, QQ;
  int i, k;

  static logical firsttime = True;

  W = m;

  cvamp[1] = 2.7777777777777778E-02;
  cvamp[2] = -2.7777777777777778E-04;
  cvamp[3] = 4.7241118669690098E-06;
  cvamp[4] = -9.1857730746619636E-08;
  cvamp[5] = 1.8978869988970999E-09;
  cvamp[6] = -4.0647616451442255E-11;
  cvamp[7] = 8.9216910204564526E-13;
  cvamp[8] = -1.9939295860721076E-14;
  cvamp[9] = 4.5189800296199182E-16;
  cvamp[10] = -1.0356517612182470E-17;

  k = 0;
  /* PI=3.141592653589793238; Defined elsewhere in code */
  /* 3*sqrt(hbar^2/kB/u)/1e-10/sqrt(3) */
  scale = 12.063464;
  if (TDebye <= 0.0)
    tamp = 0.0;
  else
  {
    k++;
    if (T <= 0.0)
      QX = 0.25;
    else
    {
      QZ = TDebye / T;
      QA = QZ * QZ;
      if (QZ < 2.16)
      {
        /*
                 Convergent expansion of the Debye function:
        */
        QB = 1.0 / QZ;
        QX = QB;
        for (i = 1; i <= 10; i++)
        {
          QB = QB * QA;
          QX = QX + QB * cvamp[i];
        }
      }
      else
      {
        /*
                Asymptotic expansion of the Debye function:
        */
        QB = QZ;
        QX = 0.25 + PI * PI / (6.0 * QA);
        QA = exp(-QZ);
        QQ = QA;
        for (i = 1; i <= 10; i++)
        {
          QX = QX - ((1.0 + 1.0 / QB) / QB) * QQ;
          QB = QB + QZ;
          if (QB > 160.0)
            break;
          QQ = QA * QQ;
        }
      }
    }

    tamp = scale * sqrt(QX / (W * TDebye));
  }
  if (firsttime)
  {
    firsttime = False;
    printf("First time in debyedisplacments_stegun. Obtained\n");
    printf("rms 1D displacement for mass=%gu, TDebye=%gK and T=%gK: %g A\n", W, TDebye, T, tamp);
  }

  return (tamp);
}
