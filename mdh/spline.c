#include "common.h"  //add this back in the end.. FIX
#include "src/spline.h"
/* 
  The algorithms for spline interpolation of rho and alpha, taken from Numerical Methods.

*/

real splint(real *xa, real *ya, real *y2a, int n, real x)
{
  int k,khi,klo;
  real a,b,h,y;		     

  //DEBUGSR("Entering splint function...",x);
  klo=0;
  khi=n-1;

  while (khi-klo > 1)
    {
      k=(khi+klo)/2;
      if(xa[k] > x) khi=k;
      else klo=k;
    }
  
  h=xa[khi]-xa[klo];
  if (h == 0.0)
    {
      printf("Error in splint. ");
      exit(1);  
    }
  a=(xa[khi]-x)/h;
  b=(x-xa[klo])/h;
  y=a*ya[klo]+b*ya[khi]+(pow(a,3-a)*y2a[klo]+pow(b,3-b)*y2a[khi])*pow(h,2)/6.0;
  //DEBUGSRRR("Found interpolated point y from a & b",a,b,y);
  return y;
}

int spline(real *x, real *y, int n, real yp1, real ypn, real *y2) 
{
  int i,k;
  real p,qn,sig,un,u[MAXSTOPP];  
  
  // Outputs second derivatives

  if (yp1 > .99e30)
    {
      y2[0]=0.0; // first value of sstopp & seldens are set to 0
      u[0]=0.0;
    }
  else
    {
      y2[0]=-0.5;
      u[0]=(3./(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
    }
  for (i=1;i<n-2;i++)
    {
      sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
      p=sig*y2[i-1]+2.0;
      y2[i]=(sig-1.0)/p;
      u[i]=(6.0*((y[i+1]-y[i])/(x[i+1]-x[i])-(y[i]-y[i-1])/(x[i]-x[i-1]))/(x[i+1]-x[i-1])-sig*u[i-1])/p;
    }

  if (ypn > .99e30)
    {
      qn=0.0;
      un=0.0;
    }
  else
    {
      qn=0.5;
      un=(3./(x[n-1]-x[n-2]))*(ypn-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
      //DEBUGSRR("Spline vals 2",qn,un);
    }
  y2[n-1]=(un-qn*u[n-2])/(qn*y2[n-2]+1.0);
  
  for (k=n-2;k>=1;k--)
    {
      y2[k]=y2[k]*y2[k+1]+u[k];
      //DEBUGSR("Final spline vals",y2[k]);
    }

  return 0;
}
     /*    The fortran code:
!***********************************************************************

SUBROUTINE splint(xa,ya,y2a,n,x,y)
  INTEGER :: n
  REAL :: x,y,xa(n),y2a(n),ya(n)
  INTEGER :: k,khi,klo
  REAL :: a,b,h
  klo=1
  khi=n
1 if (khi-klo.gt.1) then
     k=(khi+klo)/2
     if(xa(k).gt.x)then
        khi=k
     else
        klo=k
     endif
     goto 1
  endif
  h=xa(khi)-xa(klo)
  if (h.eq.0.) then
     write (*,*) "Error in spline"
     return
  endif
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h
  y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
  return
END SUBROUTINE splint

!***************************************************************************

SUBROUTINE spline(x,y,n,yp1,ypn,y2)
INTEGER n,NMAX
REAL yp1,ypn,x(n),y(n),y2(n)
PARAMETER (NMAX=2000)
INTEGER i,k
REAL p,qn,sig,un,u(NMAX)
if (yp1.gt..99e30) then
y2(1)=0.
u(1)=0.
else
y2(1)=-0.5
u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
endif
do i=2,n-1
sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
p=sig*y2(i-1)+2.
y2(i)=(sig-1.)/p
u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
enddo

if (ypn.gt..99e30) then
qn=0.
un=0.
else
qn=0.5
un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
endif
y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
do k=n-1,1,-1
y2(k)=y2(k)*y2(k+1)+u(k)
enddo

return
END SUBROUTINE spline

     */
