#ifndef __POTENTIAL__
#define __POTENTIAL__

#include "../global.h"

void potential(
real ir,
real r,
real *V,
real *dV,
int typei,
int typej,
logical recoil,
struct pot *pot,
struct potcrit *potcrit,
struct file *file);

void splinereppot(
real ir,
real r,           /* input r */
real *V,
real  *dV,    /* Output result */
int ti,
int  tj,      /* Atom types i and j */
real screenfact, /* Coulombic scale factor Z1 Z2/4 pi eps_0 */
struct file *file);



#endif /*__POTENTIAL__*/