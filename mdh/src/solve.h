#ifndef __SOLVE__
#define __SOLVE__

#include "../global.h"

void solve1(
struct atom *at,
struct time *time,
struct atomflag *atflag,
struct box *box,
int nat,
int Ntypes);

void solve2(
struct atom *at,
struct time *time,
struct atomflag *atflag,
struct box *box,
int nat,
int Ntypes);

void subtractcmacc(
struct atom *at,
int nat);

void subtractcmvel(
struct atom *at,
int nat);

void setcelltemp(
struct atom *at,
struct atomflag *atflag,
int nat,
struct unit *unit,
real T);

void getcelltemp(
struct atom *at,
struct atomflag *atflag,
int nat,
struct unit *unit,
real *T,
real *ekin);

#endif /*__SOLVE__*/