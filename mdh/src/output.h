#ifndef __OUTPUT__
#define __OUTPUT__

#include "../global.h"

void printcoveac(struct atom *at, int nat, real t);
void printco(struct atom* at, int nat, struct box* box,real t, int n);
void printcoint(struct atom* at, struct atomflag *atflag, int nat, struct box* box,real t, int n);
void printrec(struct recoil *recoil,real t, int n);
void eltime();
void oflush();

#endif /*__OUTPUT__*/