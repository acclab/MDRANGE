#ifndef __ACCELERATIONS__
#define __ACCELERATIONS__

#include "../global.h"

void accelerations(
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
struct pot *pot,
struct potcrit *potcrit,
struct box *box,
struct physical *physical, 
struct unit *unit,
int nat,int mode,
logical *update,
struct file *file);

void checkdisplacements(
logical *updateneeded,
point *updatepos,
struct atom *at,
struct atomflag *atflag,
int nat,
struct potcrit *potcrit,
struct box *box,
int mode);

void getngbrlist(
int *G,
int *NG,
logical updateneeded,
point *updatepos,
int mode,
struct atom *at,
struct atomflag *atflag,
int nat,
struct potcrit *potcrit,
struct box *box);

void getaccs(
int G[],
int NG,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
struct pot *pot,
struct potcrit *potcrit,
struct box *box,
struct physical *physical,
struct unit *unit,
int nat,
int mode,
struct file *file);


#endif /*__ACCELERATIONS__*/