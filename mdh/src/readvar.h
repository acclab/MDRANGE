#ifndef __READVAR__
#define __READVAR__

#include "../global.h"
#include <stdio.h>


int readi(
char *varname,
FILE *fp);

int readic(
char *varname,
int *v,
int v0,
char *mess,
FILE *fp,
FILE *fps);

float readr(
char *varname,
FILE *fp);

int readrc(
char *varname,
float *v,
float v0,
char *mess,
FILE *fp,
FILE *fps);

double readd(
char *varname,
FILE *fp);

int readdc(
char *varname,
double *v,
double v0,
char *mess,
FILE *fp,
FILE *fps);

char *reads(
char *varname,
FILE *fp);

int readsc(
char *varname,
char *v,
char*v0,
char *mess,
FILE *fp,
FILE*fps);

char *asplit(
char *str,
char *indstr,
int ind); 

int readvar(
char *varname,
int *in,
double *fl,
char *str,
int flag,
FILE *fp);

int skipspaces(
FILE *fp);

void searchname(
FILE *fp,
char *varname,
int l,
int *n,
int *nsearched,
int *pos);

int sequal(
char *s1,
char *s2,
int l);

#endif /*__READVAR__*/