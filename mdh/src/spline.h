#ifndef __SPLINE__
#define __SPLINE__

#include "../global.h"

real splint(real *xa, real *ya, real *y2a, int n, real x);

int spline(real *x, real *y, int n, real yp1, real ypn, real *y2);




#endif /*__SPLINE__*/