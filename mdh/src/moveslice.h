#ifndef __MOVESLICE__
#define __MOVESLICE__

#include "../global.h"

int moveslice(
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
struct type type[MAXTYPE],
struct iniat *iniat,
struct layers *layers,
int *nlayer,
int *nat,
struct box *box,
struct recoil *recoil,
struct reccalc *reccalc,
struct recresult *recresult,
logical *update,
logical firstrecstep,
logical recoilspec,
struct physical *physical,
struct unit *unit,
struct file *file);

void moveineg(
real *coord,
real lim, 
real box,
int i, 
int j,
logical *l);

void moveipos(
real *coord,
real lim, 
real box,
int i, 
int j,
logical *l);

void copyiniat( 
int i,
int  j,
struct iniat *iniat,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
int nlayer,
int  ninistate,
int namorph);

void copyiniat2(
int i,
int j,
struct iniat *iniat,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
int nlayer, 
int ninistate,
int namorph,
real x,
real y,
real z);

void copyatom(
struct atom *at1, 
struct atomex *atex1,
struct atomflag *atflag1,
struct atom *at2,
struct atomex *atex2,
struct atomflag *atflag2);

void zeroatom(
struct atom *at,
struct atomex *atex,
struct atomflag *atflag);

#endif /*__MOVESLICE__*/