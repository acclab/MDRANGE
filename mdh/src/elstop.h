#ifndef __ELSTOP__
#define __ELSTOP__

#include "../global.h"

void readelstop(
struct file *file,
struct elstop *elstop,
struct type *type,
struct layers *layers,
struct reccalc *reccalc);

void subelstop(
struct atom *at,
struct time *time,
struct recoil *recoil,
struct elstop *elstop,
struct unit *unit,
struct type *type,
struct layer layer,
struct potcrit *potcrit,
int nlayer,
struct file *file,
int *minindex_prev,
real *current_dist_remaining,
int nat);

real LSSOmega(
int Z1,
real Z2,
real v,
real N,
real deltar,
struct unit *unit,
int t);

int search(ZToIndex* dictionary, int key);

int getAvgLatticeProps(
struct layer* layer, 
struct atom *at, 
struct potcrit *potcrit, 
struct type *type,
struct elstop *elstop,
int nat);

void setEPHInnerRadius(
struct layers *layers,
struct potcrit *potcrit);


#endif /*__ELSTOP__*/