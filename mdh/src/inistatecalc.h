#ifndef __INISTATECALC__
#define __INISTATECALC__

#include "../global.h"

void inistatecalc(
struct iniat *iniat,
struct pot *pot,
struct potcrit *potcrit,
struct time *time,
struct unit *unit,
struct type *type,
struct file *file,
struct box *box,
struct physical *physical,
struct layers *layers,
int Ntypes);

void gaussianvels(
struct iniat *iniat,
struct type type[MAXTYPE],
struct file *file,
struct unit *unit,
struct layers *layers,
real T);

void copyinilayer(
struct iniat *iniat,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
int nlayer,
int ninistate,
int namorph,
int nat);

void setinilayer(
struct iniat *iniat,
struct atom *at,
int nlayer,
int ninistate,
int namorph,
int nat);

real debyedisplacements_stegun(
real T,
real TDebye,
real m);





#endif /*__INISTATECALC__*/