#ifndef __BORDERS__
#define __BORDERS__

#include "../global.h"

void erodesurface(
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
struct atom *attemp,
struct atomex *atextemp,
struct atomflag *atflagtemp,
struct recoil *recoil,
struct physical *physical,
struct box *box,
int *nat);


void borders(
struct atom *at,
struct box *box,
int nat);

void getamorphstate(
int *namorph,
struct recresult *recresult,
real recoilz,
real boxz,
struct physical *physical,
struct layers *layers,
int nlayer);

void spread_fdn(
real r,
real boxz,
struct recresult *recresult,
real fdn,
int model);

void check_depencalc(
struct physical *physical,
struct elstop *elstop,
struct recoil *recoil,
struct reccalc *reccalc,
struct box *box,
struct poly *poly,
struct recresult *recresult,
struct layers *layers,
int n,
real *Ymean);

void getlayer(
struct layers *layers,
real z,
int *nlayer,
logical firstrecstep);

void change(
struct atom *at,
struct type type[MAXTYPE],
int i);

real sqdist(
real x1,
real y1,
real z1,
real x2,
real y2,
real z2,
struct box *box);

real dist(
real x1,
real y1,
real z1,
real x2,
real y2,
real z2,
struct box *box);

void getmeandisplacement(
real *displ,
point *array,
struct atom *at,
int nat,
struct box *box);



#endif /*__BORDERS__*/