#ifndef __START__
#define __START__

#include "../global.h"

void setfiledef(
struct file *file);

void handleargs(
int argc,
char **argv,
struct file *file,
struct reccalc *reccalc);

void start(
char *version,
struct file file);

void printhost(struct file file);

/*******************************************/
/*      Functions for memory handling      */
/*******************************************/
void structAlloc(
struct file *file,
struct recresult *recresult,
struct elstop *elstop);

void structFree(
struct file *file,
struct recresult *recresult,
struct elstop *elstop);

#endif /*__START__*/