#ifndef __RANDOM__
#define __RANDOM__

#include "../global.h"

void sgenrand(unsigned long seed);
real unirand(int seed);
real oldunirand(int seed);
real gaussianrand();


#endif /*__RANDOM__*/