#ifndef __MYSPLINE__
#define __MYSPLINE__

#include "../global.h"

int spline(double *x, double *y, int n, double yp1, double ypn, double *y2);
double splint(double *xa, double *ya, double *y2a, int n, double x);

#endif /*__MYSPLINE__*/