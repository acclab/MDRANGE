#ifndef __INITPARAMS__
#define __INITPARAMS__

#include "../global.h"

void initparams(
struct type *type,
struct unit *unit,
struct time *time,
struct gen *gen,
struct file *file);

void initunits(
struct type *type,
struct unit *unit,
struct file *file);



#endif /*__INITPARAMS__*/