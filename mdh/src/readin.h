#ifndef __READIN__
#define __READIN__

#include "../global.h"

void readin(
struct type *type,
struct time *time,
struct layers *layers,
struct potcrit *potcrit,
struct pot *pot,
struct recoil *recoil,
struct reccalc *reccalc,
struct box *box,
struct physical *physical,
struct file *file,
struct gen *gen,
struct elstop *elstop);

void readtime(
struct file *file,
struct time *time);

void readtype(
struct file *file,
struct type *type,
struct reccalc *reccalc);

void readlayers(
struct file *file,
struct layers *layers);

void readpot(
struct file *file,
struct pot *pot,
struct type *type);

void readbox(
struct file *file,
struct box *box);

void readreccalc(
struct file *file,
struct reccalc *reccalc,
struct box *box);

void readpotcrit(
struct file *file,
struct potcrit *potcrit,
struct box *box);

void readphysical(
struct file *file,
struct physical *physical);

void readgen(
struct file *file,
struct gen *gen);

void readfile(
struct file *file);

void readelstopparam(
struct file *file,
struct elstop *elstop,
struct reccalc *reccalc);

#endif /*__READIN__*/