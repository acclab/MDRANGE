#ifndef __READCOORDS__
#define __READCOORDS__

#include "../global.h"

int borders_for_iniat(
real *coord,
real lim);

void readcoords(
struct iniat *iniat,
struct layers *layers,
struct file *file,
struct type *type,
struct box *box,
struct unit *unit,
struct physical *physical);

#endif /*__READCOORDS__*/