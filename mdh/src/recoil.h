#ifndef __RECOIL__
#define __RECOIL__

#include "../global.h"

void initrec(
struct file *file,
struct reccalc *reccalc,
struct recresult *recresult,
struct poly *poly,
struct recoil *recoil,
struct unit *unit,
struct elstop *elstop,
struct type *type);

void startrec(
struct iniat *iniat,
struct atom *at,
struct atomex *atex,
struct atomflag *atflag,
struct layers *layers,
int *nlayer,
int *ninistate,
struct box *box,
struct potcrit *potcrit,
struct time *time,
struct reccalc *reccalc,
struct recresult *recresult,
struct poly *poly,
struct elstop *elstop,
int *namorph);

void createrecatom(
struct reccalc *reccalc,
struct recoil *recoil,
struct maxima *maxima,
int *nat,
struct atom *at,
struct atomflag *atflag,
struct poly *poly,
int num,
struct file *file);

void getrecoilprop(
struct recoil *recoil,
struct reccalc *reccalc,
struct atom *at,
struct atomex *atex,
struct type *type,
struct box *box,
struct poly *poly);

void getmaximumprop(
struct maxima *maxima,
struct atom *at,
int nat);

void calcmisc(
struct recoil *recoil,
struct elstop *elstop,
struct reccalc *reccalc,
struct recresult *recresult,
struct poly *poly,
struct file *file,
logical firstrecstep,
real t,
struct physical *physical);


void endrec(
struct atom *at,
struct recoil *recoil,
struct reccalc *reccalc,
struct recresult *recresult,
struct potcrit *potcrit,
struct poly *poly,
struct file *file,
int *nat,
int nrec,
logical recerror, 
logical recbound,
struct physical *physical,
logical issput,
struct box *box,
struct unit *unit,
int num_bins);

void printresults(
struct file *file,
struct reccalc *reccalc,
struct recresult *recresult,
struct unit *unit,
struct physical *physical,
struct box *box,
struct elstop *elstop,
struct potcrit *potcrit,
int nrec);

void getestat(
struct recoil *recoil,
struct reccalc *reccalc,
struct file *file,
logical printflag);

int eind(int i,int j);

int find(int i,int j);

int tind(int i,int j);

void getpolysize(
struct poly *poly,
struct reccalc *reccalc);

void exitpolydomain(
struct poly *poly,
struct recoil *recoil);

#endif /*__RECOIL__*/