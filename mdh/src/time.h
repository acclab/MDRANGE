#ifndef __TIME_H__
#define __TIME_H__

void inittime(
struct type *type,
struct unit *unit,
struct time *time);

void inittsteps(
struct time *time,
struct unit *unit);

void gettsteps(
struct time *time,
struct unit *unit,
logical new);

void calctsteps(
struct time *time,
struct unit *unit,
struct maxima *maxima,
struct recoil *recoil,
logical new);
#endif /*__TIME_H__*/