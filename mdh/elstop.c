#include "common.h"
#include "src/elstop.h"

/*   BS   */
int search(ZToIndex *dictionary, int key)
{
	for (int q = 0; q < MAXTYPE; q++)
	{
		if (dictionary[q].key == key)
		{
			return dictionary[q].value;
		}
	}
	return -1; 
}
ZToIndex dictIndex[MAXTYPE]; /* Dict. converts Z to array positions*/

void readelstop(
	struct file *file,
	struct elstop *elstop,
	struct type *type,
	struct layers *layers,
	struct reccalc *reccalc)
{
	int i, n, j, counts, k, l, jakaja, ii;
	char buf[170], fname[140],fnamefinal[240]; /* KN Jan 2025 added fnamefinal */
	real a, b, alfa, var;

	char *p;

	elstop->penr = 0;

	for (j = 0; j < layers->Nlayers; j++) /*layer loop*/
	{
		printf("type of elstop for layer %d is %d\n", j, layers->layer[j].elstop);
		if (layers->layer[j].fulldensity)
		{
			printf("Allocating %lud bytes for charge density table in layer %d\n",
				   MAXRHO * MAXRHO * MAXRHO * sizeof(real), j);

			elstop->rho = (real *)malloc(MAXRHO * MAXRHO * MAXRHO * sizeof(real));

			counts = 0;
			printf("Reading charge density file for full box..");
			file->fprho = fopen(file->crho, "r");
			if (file->fprho == NULL)
				printf("Can't read %s\n", file->crho);

			for (i = 0; i < MAXRHO; i++)
				for (l = 0; l < MAXRHO; l++)
					for (k = 0; k < MAXRHO; k++)
					{
						ii = fscanf(file->fprho, "%lg", &alfa);
						elstop->rho[i + (l * MAXRHO) + (k * MAXRHO * MAXRHO)] = alfa;
						counts++;
					}
			fclose(file->fprho);
			printf("DONE, %d points\n", counts);
		}

		/*elstop type loop*/
		if ((layers->layer[j].elstop == 0)) /*max number of zbl-layers=ORIGMAXLAYER*/
		{
			strcpy(fname, file->elstop);
			strcpy(fnamefinal,fname);
			if (layers->Nlayers > 1) /*if multiple layers, elstop.in.[1,2,3..], else elstop.in*/
				sprintf(fnamefinal, "%s.%d", fname, j + 1);
			file->fpel = fopen(fnamefinal, "r");
			if (file->fpel == NULL)
				fprintf(fpdebug, "Can't read elstop(ZBL) file %s\n", fnamefinal);
			if (file->fpel == NULL && layers->Nlayers < 2) /*only one layer, but no elstop.in, trying elstop.in.0 */
			{
				sprintf(fnamefinal, "%s.%d", fname, j);
				fprintf(fpdebug, "Trying to open elstop(ZBL) file %s\n", fnamefinal);
				file->fpel = fopen(fnamefinal, "r");
				if (file->fpel == NULL)
					fprintf(fpdebug, "Can't read elstop(ZBL) file %s\n", fnamefinal);
			}

			i = 0;
			while (fgets(buf, 160, file->fpel) != NULL)
			{
				if (buf[0] == '%' || buf[0] == '!' || buf[0] == '\n')
					continue;
				n = sscanf(buf, "%lf %lf", &a, &b);
				if (n < 2)
					break;
				elstop->v[j][i] = a;
				elstop->Setable[j][i] = b;
				i++;
				if (i >= MAXELSTOP)
				{
					fprintf(fpdebug, "Too many points in elstop array, trying to manage with less, increase MAXELSTOP\n");
					fflush(fpdebug);
					break;
				}
			}
			elstop->n[j] = i;
			fprintf(file->fpo, "Read in %d elstop(ZBL) points from %s for layer %d , max vel %g max Se %g\n",
					elstop->n[j], fnamefinal, j, elstop->v[j][i - 1], elstop->Setable[j][i - 1]);
		}

		/* puska or BK, same el.density file*/
		if (layers->layer[j].elstop == 1 || layers->layer[j].elstop == 2)
		{
			elstop->penr++;
			if (layers->layer[j].fulldensity == 0) /*if not full density file provided*/
			{
				/*this checks if the ion type is also occuring in the layers!!*/
				l = 1;
				for (k = 0; k < ORIGMAXLAYER; k++)
					if (file->layeratoms[0][k] != 0)
						l = 0;

				layers->layer[j].steadydens = 0;
				jakaja = 0; /*counts the number of types in layer, needed in steadydens.*/

				for (k = l; k < MAXTYPE; k++)		 /*from 1, if type 0 is only the ion, from 0 if not !*/
					if (file->layeratoms[k][j] != 0) /*if this type (k) of atom is in this layer (j)*/
					{
						strcpy(fname, file->cdens);
						sprintf(fnamefinal, "%s.%d", fname, k);

						file->fpcdens = fopen(fnamefinal, "r");
						if (file->fpcdens == NULL)
						{
						        fprintf(fpdebug, "Can't read charge density file %s\n", fnamefinal);
							strcpy(fnamefinal, file->cdens);
							fprintf(fpdebug, "Trying to open file %s\n", fnamefinal);
							file->fpcdens = fopen(fnamefinal, "r");
							if (file->fpcdens == NULL)
							{
								fprintf(fpdebug, "Can't read charge density file %s\n", fnamefinal);
								fprintf(fpdebug, "No charge density for atom type %d in the layer %d, hopefully its OK?\n", k, j);
								goto jumpty;
							}
							else
								fprintf(fpdebug, "Using %s for atom type %d in layer %d, hopefully its OK?\n", fnamefinal, k, j);
						}
						i = 0;
						while (fgets(buf, 160, file->fpcdens) != NULL)
						{
							if (buf[0] == '%' || buf[0] == '!' || buf[0] == '\n')
								continue;
							n = sscanf(buf, "%lg %lg", &a, &b);
							if (n < 2)
								break;
							elstop->cdist[k][i] = a;
							elstop->celdens[k][i] = b;
							i++;
							if (i >= MAXSTOPP)
							{
								fprintf(fpdebug, "Too many points in cdens array, trying to manage with less, increase MAXSTOPP\n");
								fflush(fpdebug);
								break;
							}
						}
						elstop->nc[k] = i;
						layers->layer[j].steadydens += elstop->celdens[k][i - 1];
						jakaja++;
						fprintf(file->fpo, "Read in %d cdensity points from %s for layer %d atomtype %d, last density=%g\n", elstop->nc[k], fnamefinal, j, k, elstop->celdens[k][i - 1]);
					jumpty:;
					}
				if (jakaja > 0)
					layers->layer[j].steadydens /= jakaja;
				else
					layers->layer[j].steadydens = 0;
				fprintf(file->fpo, "Steady el.dens in layer %d is %lg\n", j, layers->layer[j].steadydens);
			}

			/*puska, stopping.in is Se versus r_s*/
			if (layers->layer[j].elstop == 1)
			{
				file->fpstopp = fopen(file->stopp, "r");
				if (file->fpstopp == NULL)
					fprintf(fpdebug, "Can't read stopping vs. rs file %s\n", file->stopp);

				i = 0;
				while (fgets(buf, 160, file->fpstopp) != NULL)
				{
					if (buf[0] == '%' || buf[0] == '!' || buf[0] == '\n')
						continue;
					n = sscanf(buf, "%lf %lf", &a, &b);
					if (n < 2)
						break;
					elstop->strs[0][i] = a;
					elstop->stopp[0][i] = b;
					if (i == 1)
						elstop->slim = (elstop->strs[0][1] - elstop->strs[0][0]);
					if (i > 1)
					{
						if (100 * fabs(elstop->slim - elstop->strs[0][i] + elstop->strs[0][i - 1]) > elstop->slim)
							elstop->slim = 0;
					}
					i++;
					if (i >= MAXSTOPP)
					{
						fprintf(fpdebug, "Too many points in stopping vs. rs array, trying to manage with less, increase MAXSTOPP\n");
						fflush(fpdebug);
						break;
					}
				}
				elstop->ns[0] = i;
				fprintf(file->fpo, "Read in %d el.stopping points from %s for layer %d , last stopping=%g\n",
						elstop->ns[0], file->stopp, j, elstop->stopp[0][i - 1]);
			}
			else if (layers->layer[j].elstop == 2)
			{
				/*BK, elstop->stopp[0][] is now G*S_lin versus r_s, which we precalculate now*/
				b = 6.0 / MAXSTOPP; /*maximum r_s is 6.0 for G (jussi's thesis)*/
				alfa = pow((4.0 / (9.0 * PI)), 0.3333333);
				for (i = 0; i < MAXSTOPP; i++)
				{
					a = (i + 1) * b;
					elstop->strs[0][i] = a;
					elstop->stopp[0][i] = 2.0 / (3.0 * PI) * (log(1.0 + PI / (alfa * a)) - 1.0 / (1.0 + alfa * a / PI));
					elstop->stopp[0][i] *= 1.0 + 0.717 * a - 0.125 * a * a - 0.0124 * a * a * a + 0.00212 * a * a * a * a;
					elstop->stopp[0][i] *= 2.34951e-05; /* unit conversion??*/
					if (i == 1)
						elstop->slim = (elstop->strs[0][1] - elstop->strs[0][0]);
					if (i > 1)
						if (100 * fabs(elstop->slim - elstop->strs[0][i] + elstop->strs[0][i - 1]) > elstop->slim)
							elstop->slim = 0;
				}
				elstop->ns[0] = MAXSTOPP;
				fprintf(file->fpo, "Precalculated %d el. stopping points for layer %d , last value=%g\n",
						elstop->ns[0], j, elstop->stopp[0][i - 1]);
			}

			if (elstop->slim != 0)
				elstop->iperslim = 1.0 / elstop->slim; /*needed in the future, just a speedup */
			else
				elstop->iperslim = 1.0;

			printf("Distance between stopping density points=%lg\n", elstop->slim);

		} /*puska,BK if*/

		/* --For EPH model --*/
		if (layers->layer[j].elstop == 3) /*tensor friction in W, Read in alpha(rho) and rho(r) as tables. (AES)*/ // GPK - doublecheck innerrad approx and remove version 4
		{
			/* BS - Updated for multiple files */
			char fnameImport[140];
			char filetemp[140]; /* to not overwrite cdens */
			char buffer[16];

			/* Get cdens files for each atom type */
			for (int q = 0; q < reccalc->Ntypes + 1; q++)
			{
				if ((q > 0) && (type[q].Z == type[0].Z))
					continue; /* Avoids duplicates */

				dictIndex[q].key = type[q].Z; /* Add to dictionary */
				dictIndex[q].value = q;

				sprintf(buffer, ".%d", type[q].Z);

				/*-------------------CDENS FILE-------------------*/
				strcpy(filetemp, file->cdens);
				strcpy(fnameImport, strcat(filetemp, buffer)); /* Name of cdens file to import */

				/*Read in rho(r) as table*/
				/*Using same cdens structure as puska and BK for rho(r) file*/
				file->fpcdens = fopen(fnameImport, "r");
				if (file->fpcdens == NULL)
				{
					fprintf(fpdebug, "Can't read electron density file %s\n", fnameImport);
					fprintf(fpdebug, "No charge density in the layer %d, hopefully its OK?\n", j);
					goto jumpn;
				}
				else
					fprintf(fpdebug, "Using %s in layer %d, hopefully its OK?\n", fnameImport, j);

				i = 0;
				while (fgets(buf, 160, file->fpcdens) != NULL)
				{
					if (buf[0] == '%' || buf[0] == '!' || buf[0] == '\n')
						continue;
					n = sscanf(buf, "%lg %lg", &a, &b);
					if (n < 2)
						break;
					elstop->cdist[q][i] = a;
					elstop->celdens[q][i] = b;
					i++;
					if (i >= MAXSTOPP)
					{
						fprintf(fpdebug, "Too many points in cdens array, trying to manage with less, increase MAXSTOPP\n");
						fflush(fpdebug);
						break;
					}
				}
				elstop->nc[q] = i;
				fprintf(file->fpo, "Read in %d cdensity points from %s for layer %d atomtype %d, last density=%g\n", elstop->nc[q], fnameImport, j, q, elstop->celdens[q][i - 1]);

				/*-------------------STOPPING FILE-------------------*/
				strcpy(filetemp, file->stopp);
				strcpy(fnameImport, strcat(filetemp, buffer)); /* Name of stopping file to import */

				/*Read in alpha(rho) as table, using Puska S_e vs rs file structure*/
				file->fpstopp = fopen(fnameImport, "r");
				if (file->fpstopp == NULL)
				{
					fprintf(fpdebug, "Can't read stopping alpha(rho) file %s\n", fnameImport);
					exit(1);
				}

				i = 0;
				//var = sqrt(98.2266 / sqrt(type[q].m));
				//DEBUGSRR(" ", var, type[q].m);
				while (fgets(buf, 160, file->fpstopp) != NULL)
				{
					if (buf[0] == '%' || buf[0] == '!' || buf[0] == '\n')
						continue;
					n = sscanf(buf, "%lf %lf", &a, &b);
					if (n < 2)
						break;
					elstop->strs[q][i] = a;
					elstop->stopp[q][i] = b; /*No change to internal unit*/
					i++;
					if (i >= MAXSTOPP)
					{
						fprintf(fpdebug, "Too many points in stopping alpha(rho) array, trying to manage with less, increase MAXSTOPP\n");
						fflush(fpdebug);
						break;
					}
				}

				elstop->ns[q] = i;
				fprintf(file->fpo, "Read in %d el.stopping points from %s for layer %d , last stopping=%g\n",
						elstop->ns[q], fnameImport, j, elstop->stopp[q][i - 1]);
			}

		jumpn:;
		} /*end tensor friction read in, AES*/

	} /*layer loop*/
}

void subelstop(
	struct atom *at,
	struct time *time,
	struct recoil *recoil,
	struct elstop *elstop,
	struct unit *unit,
	struct type *type,
	struct layer layer,
	struct potcrit *potcrit,
	int nlayer,
	struct file *file,
	int *minindex_prev,
	real *current_dist_remaining,
	int nat)
{
	int i, ihalf, n, k;
	static int iup = 0, idown = 1;
	real xprev, yprev, zprev, totalDistance, current_dist_iterating;
	real calculated_distance = 0.0;
	real calculated_distance_2 = 50.0;
	real v, vreal, vreal0, deltav, Se, Semod;
	real scale;
	real omega, LSSOmega();
	int jup = 0, jdown = 1, jjhalf;
	int jjup = 0, jjdown = 1, ty;
	real temp, charge, onelrad, kf, ktf;

	real z1, z2, alpha, ACR, BCR, SCR, fvx, fvy, fvz, firtotal = 0, avx, avy, avz; /*firsov*/
	real rxval, ryval, rzval, rxval2, ryval2, rzval2;
	real firtotalx = 0, firtotaly = 0, firtotalz = 0;
	int ain, atyp;
	real gamma, q, vr, vf; /*BK*/
	real ui, vi, ti;
	real elx, ely, elz, rex, rey, rez;
	real Etot;
	real e = 1.602189e-19, u = 1.6605655e-27;
	int xup, yup, zup, xdown, ydown, zdown;
	int axd, axu, ayd, ayu;				/*(JARKKO)*/
	real *apurho;						/*(JARKKO)*/
	real ti_ui, ti_vi, vi_ui, ti_ui_vi; /*(JARKKO)*/

	// stuff for tensorial friction model (AES)
	real *rhobar, *walpha, **rho, **rij, *w[3];
	real *rhorec, *rijrec; // specifically for the recoil
	real eij[3], vdiff[3], sigma[3], temptens[3], wdeltav[3], randvec[3], randrecvec[3], eta[3];
	int indi, indj, j, ii, ij, indrec;
	real p1, p2, r1, r2, q1, q2, vel_ati, vel_atj;
	real electronic_temp = 300;						 // Kelvin ... const ...
	real boltzman_const = 1.380649e-23 / 1.6022e-19; // per Kelvin in MDRANGE units
	real eta_factor;								 // Random forces

	static real stragglingsum = 0.0, stragglingn = 0.0;

	elstop->Se = Se = charge = 0.0;

	i = recoil->number;
	v = recoil->vel;
	vreal = v * unit->vel[recoil->attype];
	if (elstop->velmode == 1)
	{
		/* Use instead of actual velocity, a fake v calculated from Ekin+Epot */
		/* This mode may correspond better to BCA simulations */
		vreal0 = vreal;
		Etot = recoil->energy + recoil->epot;
		if (Etot < 0.0)
			Etot = 0.0;
		vreal = sqrt(2 * Etot * e / (recoil->mass * u));
		MAXDEBUGSRRRR("velmode", recoil->energy, recoil->epot, vreal, vreal0);
	}

	if (layer.elstop == 0) /*ZBL model for this layer?*/
	{
		if (debug)
			printf("Using zbl elstop at depth %g layer %d \n", recoil->sz, nlayer);
		n = elstop->n[nlayer];

		if (vreal > elstop->v[nlayer][n - 1])
		{
			printf("\nElstop array maximum exceeded in ZBL-file : vion %g vmaxelstop %g\n\n",
				   vreal, elstop->v[nlayer][n - 1]);
			exit(0);
		}

		if (!(vreal > elstop->v[nlayer][idown] && vreal < elstop->v[nlayer][iup]))
		{
			/* If the previous borders aren't valid, get two nearest
				 elements with binary search
			*/
			iup = n - 1;
			idown = 0;
			while (True)
			{
				ihalf = idown + (iup - idown) / 2;
				if (ihalf == idown)
					break;
				if (elstop->v[nlayer][ihalf] < vreal)
					idown = ihalf;
				else
					iup = ihalf;
			}
		}
		/* Calculate scaling factor using the formula scale+dscale*E+sqscale*E*E */
		scale = elstop->scale + recoil->energy * (elstop->dscale + elstop->sqscale * recoil->energy);

		/* Get elstop with a simple linear interpolation */
		Se = (elstop->Setable[nlayer][idown] * (elstop->v[nlayer][iup] - vreal) + elstop->Setable[nlayer][iup] * (vreal - elstop->v[nlayer][idown])) / (elstop->v[nlayer][iup] - elstop->v[nlayer][idown]);

		Se *= scale;

		/*
	Include straggling of electronic stopping if mode is 1
	See A. Luukkainen and M. Hautala, Radiat. Eff. 59 (1982)
		*/
		if (elstop->mode == 1 && recoil->dr > 0.0)
		{
			/* printf("elstop %g",Se); fflush(NULL);*/
			omega = LSSOmega(type[recoil->attype].Z, layer.Zmean, recoil->vel,
							 layer.ndensity, recoil->dr, unit, recoil->attype);
			Semod = gaussianrand() * omega / recoil->dr;
			MAXDEBUGSRRR("LSS mod", Se, Semod, omega);
			Se += Semod;

			stragglingsum = stragglingsum + abs(Semod);
			stragglingn = stragglingn + 1.0;
			if (((int)stragglingn) % 1000000 == 1)
			{
				printf("Latest and |average| elstop straggling %g %g %g\n", Semod, omega, stragglingsum / stragglingn);
			}
		}
	}
	else if (layer.elstop == 1) /*puska model*/
	{
		if (debug)
			printf("using Puska elstop at depth %g ", recoil->sz);

		if (layer.fulldensity == 0)
		{
			if (potcrit->fatoms == 0)
			{
				if (debug)
					printf("\nPuska model, no atoms inside rfirsov at depth %g, using default charge density ", recoil->sz);
				goto jump2;
			}

			for (k = 0; k < potcrit->fatoms; k++) /*Si-atoms inside Firsov (default=1.47) radius*/
			{

				ty = at[potcrit->fin[k]].type;
				if (!(potcrit->frmin[k] > elstop->cdist[ty][jjdown] && potcrit->frmin[k] < elstop->cdist[ty][jjup]))
				{
					/* If the previous borders(0-1) aren't valid, get two nearest
						 elements with binary search
					*/
					jjup = elstop->nc[ty] - 1;
					jjdown = 0;
					while (True)
					{
						jjhalf = jjdown + (jjup - jjdown) / 2;
						if (jjhalf == jjdown)
							break;
						if (elstop->cdist[ty][jjhalf] < potcrit->frmin[k])
							jjdown = jjhalf;
						else
							jjup = jjhalf;
					}
				}
				/* Get charge density with a simple linear interpolation */

				/*temp=1.0/(elstop->cdist[jjup]-elstop->cdist[jjdown]);*/
				charge += (elstop->celdens[ty][jjdown] * (elstop->cdist[ty][jjup] - potcrit->frmin[k]) + elstop->celdens[ty][jjup] * (potcrit->frmin[k] - elstop->cdist[ty][jjdown])) / (elstop->cdist[ty][jjup] - elstop->cdist[ty][jjdown]);
				/*SP+=elstop->celdens[jjup]*(potcrit->frmin[k]-elstop->cdist[jjdown])*temp;*/
				MAXDEBUGSRR("elstop.c", Se, potcrit->frmin[k]);
			} /*fatoms looppi*/

		jump2:;
			if (charge > 0)
			{
				charge *= 0.1481848229;
			}
			/*unit conversion: 1 e/Ongs^3 -> atomic units=1 e/(Bohrradius=0.529 Ong)^3 */
			else
			{
				charge = layer.steadydens * 0.1481848229;
			}
			/*tasainen varaustiheys 0.119e(for si)/Ong^3 ->atomic units  */

			onelrad = pow(3.0 / (4 * PI * charge), 0.33333333); /*one-electron radius from el. charge density      */
		}
		else
		{

			rex = recoil->x;
			rey = recoil->y;
			rez = recoil->z;

			if (rex <= elstop->ux) /*For silicon ux=5.43 ong */
				elx = rex;
			else
				elx = (rex - (floor(rex / elstop->ux) * elstop->ux));

			if (rey <= elstop->uy)
				ely = rey;
			else
				ely = (rey - (floor(rey / elstop->uy) * elstop->uy));

			/*rez=recoil->sz-potcrit->oxide; */
			if (rez <= elstop->uz)
				elz = rez;
			else
				elz = (rez - (floor(rez / elstop->uz) * elstop->uz));

			if (elz < 0 || elx < 0 || ely < 0)
			{
				printf("cdens variable(%lg,%lg,%lg) LESS THAN ZERO z%lg sz%lg\n", elx, ely, elz, recoil->z, recoil->sz);
				exit;
			}

			xup = (int)ceil(elx / elstop->dx);
			if (xup > 0)
				xdown = xup - 1;
			else
				xdown = 0;
			if (xup == 128)
				xup = 0;
			yup = (int)ceil(ely / elstop->dy);
			if (yup > 0)
				ydown = yup - 1;
			else
				ydown = 0;
			if (yup == 128)
				yup = 0;
			zup = (int)ceil(elz / elstop->dz);
			if (zup > 0)
				zdown = zup - 1;
			else
				zdown = 0;
			if (zup == 128)
				zup = 0;

			if (zup < 0 || zdown > 127 || xup < 0 || xdown > 127 || yup < 0 || ydown > 127)
			{
				printf("OUT OF BOUNDS ERROR %d %d %d\n", xdown, ydown, zdown);
				printf("%lg %lg %lg\n", elx, ely, elz);
				exit;
			}
			ti = (elx - (xdown * elstop->dx)) / elstop->dx;
			ui = (ely - (ydown * elstop->dy)) / elstop->dy;
			vi = (elz - (zdown * elstop->dz)) / elstop->dz;

			ayd = ydown << 7;
			ayu = yup << 7;
			axd = xdown << 14;
			axu = xup << 14;
			apurho = (real *)(elstop->rho);
			ti_ui = ti * ui;
			ti_ui_vi = ti_ui * vi;
			ti_vi = ti * vi;
			vi_ui = vi * ui;
			temp = (1 - vi - ti + ti_vi - ui + vi_ui + ti_ui - ti_ui_vi) * apurho[axd + ayd + zdown] + (ti - ti_ui - ti_vi + ti_ui_vi) * apurho[axu + ayd + zdown];
			temp += (ti_vi - ti_ui_vi) * apurho[axu + ayd + zup] + (vi - ti_vi - vi_ui + ti_ui_vi) * apurho[axd + ayd + zup];
			temp += (ui - ti_ui - vi_ui + ti_ui_vi) * apurho[axd + ayu + zdown] + (ti_ui - ti_ui_vi) * apurho[axu + ayu + zdown];
			temp += ti_ui_vi * apurho[axu + ayu + zup] + (vi_ui - ti_ui_vi) * apurho[axd + ayu + zup];
			/*(JARKKO) */
			/*
				cha=(1-ti)*(1-ui)*(1-vi)*elstop->rho[xdown][ydown][zdown]+ti*(1-ui)*(1-vi)*elstop->rho[xup][ydown][zdown];
				cha+=ti*vi*(1-ui)*elstop->rho[xup][ydown][zup]+(1-ti)*vi*(1-ui)*elstop->rho[xdown][ydown][zup];
				cha+=(1-ti)*ui*(1-vi)*elstop->rho[xdown][yup][zdown]+ti*ui*(1-vi)*elstop->rho[xup][yup][zdown];
				cha+=ti*ui*vi*elstop->rho[xup][yup][zup]+(1-ti)*ui*vi*elstop->rho[xdown][yup][zup];
			*/
			onelrad = exp(temp); /*note that rs was in reality log(rs) in atomic units!! */
		} /*charge if */

		if (elstop->correction)
			if (onelrad > 3.0)
				onelrad *= pow(5.0, -0.33333); /*efective r_s for low el. densities in channels (n<0.0088 a.u. or 0.0597 e/Ong^3)*/

		/*printf("one el.rad:%g\n",onelrad);*/
		if (onelrad > 0.1)
		{
			jup = (int)ceil(onelrad / elstop->slim);
			jdown = (int)floor(onelrad / elstop->slim);
			jdown--;
			jup--;
			if (jup >= elstop->ns[0])
			{
				printf("TOO LOW ELECTRON DENSITY number:%d, using MAX=%d, onelrad:%g\n", jup, elstop->ns[0], onelrad);
				jup = jdown = elstop->ns[0] - 1;
			}
			else if (/*jdown<0 ||*/ jup < 0)
			{
				printf("TOO HIGH ELECTRON DENSITY number:%d, using 0\n", jdown);
				jdown = jup = 0;
			}
			if (jup != jdown)
			{
				Se = (elstop->stopp[0][jdown] * (elstop->strs[0][jup] - onelrad) + elstop->stopp[0][jup] * (onelrad - elstop->strs[0][jdown])) * elstop->iperslim; /*iperslim= 1/slim*/
																																								   /*Se+=elstop->stopp[0][jup]*(onelrad-elstop->strs[0][jdown])*elstop->iperslim;*/
			}
			else
				Se = elstop->stopp[0][jdown];
		}
		else
		{ /*Linear response theory*/
			Se = type[recoil->attype].Z * type[recoil->attype].Z * 0.21220659 * (log(1 + 6.0292139 / onelrad) - (1 / (1 + 0.16585911 * onelrad)));
			Se *= 2.34951e-05; /*atomic units-> [eV] [Ong] [m/s]*/
		}
		Se *= vreal;
	}
	else if (layer.elstop == 2) /*BK-stopping,ala. Cai-Beardmore style*/
	{
		if (layer.fulldensity == 0)
		{

			if (potcrit->fatoms == 0)
			{
				if (debug)
					printf("BK model, no atoms inside rfirsov at depth %g, using default charge density ", recoil->sz);
				goto jump2bk;
			}

			if (debug)
				printf("using BK elstop at depth %g ", recoil->sz);
			for (k = 0; k < potcrit->fatoms; k++) /*Si-atoms inside Firsov (default=1.47) radius*/
			{
				ty = at[potcrit->fin[k]].type;
				if (!(potcrit->frmin[k] > elstop->cdist[ty][jjdown] && potcrit->frmin[k] < elstop->cdist[ty][jjup]))
				{
					jjup = elstop->nc[ty] - 1;
					jjdown = 0;
					while (True)
					{
						jjhalf = jjdown + (jjup - jjdown) / 2;
						if (jjhalf == jjdown)
							break;
						if (elstop->cdist[ty][jjhalf] < potcrit->frmin[k])
							jjdown = jjhalf;
						else
							jjup = jjhalf;
					}
				}

				charge += (elstop->celdens[ty][jjdown] * (elstop->cdist[ty][jjup] - potcrit->frmin[k]) + elstop->celdens[ty][jjup] * (potcrit->frmin[k] - elstop->cdist[ty][jjdown])) / (elstop->cdist[ty][jjup] - elstop->cdist[ty][jjdown]);
				MAXDEBUGSRR("elstop.c", Se, potcrit->frmin[k]);
			} /*fatoms loop*/

		jump2bk:;

			if (charge > 0)
			{
				charge *= 0.1481848229; /*printf("Sein: %g\n",charge);*/
			}
			/*unit conversion: 1 eV/ong^3 -> 1 e/(Bohrradius=0.529 Ong)^3 =0.14803589*/
			else
			{
				charge = layer.steadydens * 0.1481848229;
			}

			onelrad = pow(3.0 / (4 * PI * charge), 0.33333333); /*one-electron radius from el. charge density*/
		}
		else
		{
			/* Let's interpolate the charge density */
			rex = recoil->x;
			rey = recoil->y;
			rez = recoil->z;

			if (rex <= elstop->ux) /*For silicon ux=5.43 ong */
				elx = rex;
			else
				elx = (rex - (floor(rex / elstop->ux) * elstop->ux));

			if (rey <= elstop->uy)
				ely = rey;
			else
				ely = (rey - (floor(rey / elstop->uy) * elstop->uy));

			/*rez=recoil->sz-potcrit->oxide; */
			if (rez <= elstop->uz)
				elz = rez;
			else
				elz = (rez - (floor(rez / elstop->uz) * elstop->uz));

			if (elz < 0 || elx < 0 || ely < 0)
			{
				printf("cdens variable(%lg,%lg,%lg) LESS THAN ZERO z%lg sz%lg\n", elx, ely, elz, recoil->z, recoil->sz);
				exit;
			}

			xup = (int)ceil(elx / elstop->dx);
			if (xup > 0)
				xdown = xup - 1;
			else
				xdown = 0;
			if (xup == 128)
				xup = 0;
			yup = (int)ceil(ely / elstop->dy);
			if (yup > 0)
				ydown = yup - 1;
			else
				ydown = 0;
			if (yup == 128)
				yup = 0;
			zup = (int)ceil(elz / elstop->dz);
			if (zup > 0)
				zdown = zup - 1;
			else
				zdown = 0;
			if (zup == 128)
				zup = 0;

			if (zup < 0 || zdown > 127 || xup < 0 || xdown > 127 || yup < 0 || ydown > 127)
			{
				printf("OUT OF BOUNDS ERROR %d %d %d\n", xdown, ydown, zdown);
				printf("%lg %lg %lg\n", elx, ely, elz);
				exit;
			}
			ti = (elx - (xdown * elstop->dx)) / elstop->dx;
			ui = (ely - (ydown * elstop->dy)) / elstop->dy;
			vi = (elz - (zdown * elstop->dz)) / elstop->dz;

			/*(JARKKO) optimointiehdotus,jonka kanssa(50kev Al->100 500t) 7.87 cpu/ion,ilman 7.46 ??*/
			/*kuitenkin 5000t, kanssa:7.20 cpu/ion, -> pidetaan paalla*/

			ayd = ydown << 7;
			ayu = yup << 7;
			axd = xdown << 14;
			axu = xup << 14;
			apurho = (real *)(elstop->rho);
			ti_ui = ti * ui;
			ti_ui_vi = ti_ui * vi;
			ti_vi = ti * vi;
			vi_ui = vi * ui;
			onelrad = (1 - vi - ti + ti_vi - ui + vi_ui + ti_ui - ti_ui_vi) * apurho[axd + ayd + zdown] + (ti - ti_ui - ti_vi + ti_ui_vi) * apurho[axu + ayd + zdown];
			onelrad += (ti_vi - ti_ui_vi) * apurho[axu + ayd + zup] + (vi - ti_vi - vi_ui + ti_ui_vi) * apurho[axd + ayd + zup];
			onelrad += (ui - ti_ui - vi_ui + ti_ui_vi) * apurho[axd + ayu + zdown] + (ti_ui - ti_ui_vi) * apurho[axu + ayu + zdown];
			onelrad += ti_ui_vi * apurho[axu + ayu + zup] + (vi_ui - ti_ui_vi) * apurho[axd + ayu + zup];

			onelrad = exp(onelrad); /*note that rs was in reality log(rs)!! */
		} /*charge if */

		if (elstop->correction)
			if (onelrad > 3.0)
				onelrad *= pow(5.0, -0.33333); /*efective r_s for low el. densities in channels (n<0.0088a.u. or 0.0597 e/Ong^3)*/

		/*calculating gamma, Z_eff=gamma*Z1*/
		if (type[recoil->attype].Z > 1)
		{
			vf = 1.9191583 / onelrad; /* =1/(alfa*r_s)  (?)*/
			vr = vreal / 2187640;
			if (vr < vf)
				q = 0.75 * vf * (1 + (2 * vr * vr / (3 * vf * vf)) - (vr * vr * vr * vr / (15 * vf * vf * vf * vf)));
			else
				q = vf * (1 + (vf * vf / (5 * vr * vr)));

			/*q is now the relative velocity*/

			q *= 1 / pow(1.0 * type[recoil->attype].Z, 0.66667); /*q is now reduced relative vel.*/
			q = 1 - exp(-0.95 * (q - 0.07));					 /*q is now charge fraction*/
			/*q=0.89*q-0.3*q*q;	 Klein et al., APL 57 , neglible difference(jussi's thesis)*/
			gamma = 2 * 0.24005 * pow((1 - q), 0.66667) / (pow(1.0 * type[recoil->attype].Z, 0.33333) * (1 - ((1 - q) / 7))); /*BK:s screening length lambda*/
			kf = onelrad * pow((9.0 * PI) / 4, 0.33333);
			ktf = sqrt(4 * kf / PI);
			gamma = gamma / (1.0 - (2.0 / 3.0) * ktf * ktf * gamma * gamma); /*Kaneko, improvement improvement Prev. A41, 4889 (1990)*/
			gamma = q + 0.5 * (1 - q) * log(1 + (16 * gamma * gamma / (onelrad * onelrad)));
			gamma *= type[recoil->attype].Z; /* Zeff calculated - square */
			gamma *= gamma;
		}
		else
			gamma = 1;

		jup = (int)ceil(onelrad / elstop->slim);
		jdown = (int)floor(onelrad / elstop->slim);
		jup--;
		jdown--;
		if (jup >= elstop->ns[0])
		{
			printf("TOO LOW DENSITY %d\n", jup);
			jup = jdown = elstop->ns[0] - 1;
		}
		if (jdown < 0)
		{
			printf("TOO HIGH DENSITY %d\n", jdown);
			jdown = jup = 0;
		}

		/*Now elstop->stopp[0] must be the correction factor G(r_s)*/
		if (jup != jdown)
		{
			Se = elstop->stopp[0][jdown] * (elstop->strs[0][jup] - onelrad) / elstop->slim;
			Se += elstop->stopp[0][jup] * (onelrad - elstop->strs[0][jdown]) / elstop->slim;
		}
		else
			Se = elstop->stopp[0][jdown];

		Se *= vreal;
		/* Calculate scaling factor using the formula scale+dscale*E+sqscale*E*E */
		scale = elstop->scale + recoil->energy * (elstop->dscale + elstop->sqscale * recoil->energy);
		Se *= scale; /* We scale the stopping of proton, default value: scale=1 */
		Se *= gamma; /* and multiply it by the square of the effective charge to get the stopping of a heavy ion (gamma=1 for hydrogen)*/
	}

	else if (layer.elstop == 3) 
	
			/* Tensorial friction, doesn't currently work with damage calc. nor should the Firsov model be used in conjunction.

			General Considerations:
				- The model may still have bugs.
			 	- cdens.in ( interatomic distance | electron density )
				- cdist stores the distances from the cdens.in file (first column)
				- celdens stores the densities from the cdens.in file (second column)
				- elstop->nc[0] = the length of the cdens.in file
				- density values from cdens.in are read in with TWO INDICES [k = atom type][i = index] 
				- stopping.in is alpha(rho) file 
				- 'spline' (density and alpha spline) 

				- wcut should be specified, which is the cutoff value of interatomic distance.
				- The "inner radius" approximation is used, whereby the atoms in the immediate vicinity of the recoil calculate all interactions.
				- Atoms outside of this inner radius experience an "average" effect for the w[...] and rhobar.
				- For this mode to work, ensure innerrad <  wcut.
				- For the "full" EPH model without the inner radius approximation, set innerrad=wcut.

				- Currently most user-defined variables are assigned through potcrit struct. 
				- EPH model requires a larger r0 value than was previously used for potential calculations in accelerations.c, performance is reduced.
				- Ensure to include cdens.in.ATOMICNUM and stopping.in.ATOMICNUM files in the directory (e.g. Si: cdens.in.14, stopping.in.14).
				- If using layers with other atom types, also provide cdens and stopping input for these. These other atoms can be included in average calculation upon changing layers. 
			AES GPK BS */

	{
		for (ii = 0; ii < 3; ii++)
			w[ii] = (real *)calloc((potcrit->watoms + 1), sizeof(real));

		rhobar = (real *)calloc((potcrit->watoms + 1), sizeof(real));
		walpha = (real *)calloc((potcrit->watoms + 1), sizeof(real));

		rijrec = (real *)malloc((potcrit->watoms + 1) * sizeof(real));
		rhorec = (real *)malloc((potcrit->watoms + 1) * sizeof(real));

		rij = (real **)malloc((potcrit->watoms + 1) * sizeof(real *));
		for (ii = 0; ii < potcrit->watoms + 1; ii++)
			rij[ii] = (real *)calloc((nat + 1), sizeof(real)); 
		rho = (real **)malloc((potcrit->watoms + 1) * sizeof(real *));
		for (ii = 0; ii < potcrit->watoms + 1; ii++)
			rho[ii] = (real *)calloc((nat + 1), sizeof(real));

		vreal = v * unit->vel[recoil->attype];

		sigma[0] = 0.0;
		sigma[1] = 0.0;
		sigma[2] = 0.0;
		eta[0] = 0.0;
		eta[1] = 0.0;
		eta[2] = 0.0;

		int currID;
		int recoilID;
		int jID;
		real tmp, tmp_units;
		real squaredlim = potcrit->wcut * potcrit->wcut;

		recoilID = search(dictIndex, type[at[i].type].Z);
		indrec = potcrit->watoms;

		for (j = 0; j < potcrit->watoms; j++)
		{
			indj = potcrit->win[j]; 	// looping over the atoms in array win

			jID = search(dictIndex, type[at[indj].type].Z);
			rijrec[j] = potcrit->watomdist[j];																										// distance to recoil stored
			rij[j][i] = rijrec[j];																													// distance to recoil symmetric
			rhorec[j] = splint(elstop->cdist[jID], elstop->celdens[jID], elstop->celdens[jID], elstop->nc[jID], rijrec[j]);							// effect of atom j on recoil
			rho[j][i] = splint(elstop->cdist[recoilID], elstop->celdens[recoilID], elstop->celdens[recoilID], elstop->nc[recoilID], rij[j][i]); 	// effect of recoil on atom j

			rhobar[indrec] = rhobar[indrec] + rhorec[j]; 	// effect of j on i (recoil)
			rhobar[j] = rhobar[j] + rho[j][i];			 	// effect of i on j (atom j in watoms)

			if (rijrec[j] < potcrit->innerrad) 		// [nlayer]
			{									 	// full neighbour calculation for atoms inside the "inner radius" 
				for (ii = 0; ii < nat - 1; ii++) 	// rho vals have been calculated for the projectile-atom pair already. All other atom contributions calculated here.
				{
					if (ii != indj)
					{
						rxval = at[indj].x - at[ii].x;
						rxval2 = rxval * rxval;
						if (rxval2 > squaredlim)
						{
							rij[j][ii] = 30.0; 	// arbitrary
							rho[j][ii] = 0.0;  	// arbitrary
							continue;
						}

						ryval = at[indj].y - at[ii].y;
						ryval2 = ryval * ryval;
						if (ryval2 + rxval2 > squaredlim)
						{
							rij[j][ii] = 30.0; 	// arbitrary
							rho[j][ii] = 0.0;  	// arbitrary
							continue;
						}

						rzval = at[indj].z - at[ii].z;
						rzval2 = rzval * rzval;
						if (rzval2 + ryval2 + rxval2 > squaredlim)
						{
							rij[j][ii] = 30.0; 	// arbitrary
							rho[j][ii] = 0.0;  	// arbitrary
							continue;
						}

						rij[j][ii] = sqrtf((rzval2 + ryval2 + rxval2));

						currID = search(dictIndex, type[at[ii].type].Z);
						rho[j][ii] = splint(elstop->cdist[currID], elstop->celdens[currID], elstop->celdens[currID], elstop->nc[currID], rij[j][ii]); 	// effect of the atoms ii on the potcrit atoms j
						rhobar[j] = rhobar[j] + rho[j][ii];
					}
				}
			} 	// Loop done for inner atoms!
			else
			{
				for (ii = 0; ii < nat; ii++)
				{
					rij[j][ii] = 30.0; 	// arbitrary
					rho[j][ii] = 0.0;  	// arbitrary
				}
				rhobar[j] = rhobar[j] + layer.avgRhobar; 	// Average effect of w for other atoms! Effect on recoil calculated.
			}
		}

		// Walpha and w's for recoil
		if (rhobar[indrec] <= elstop->strs[recoilID][(elstop->ns[recoilID]) - 1])
		{
			walpha[indrec] = splint(elstop->strs[recoilID], elstop->stopp[recoilID], elstop->stopp[recoilID], elstop->ns[recoilID], rhobar[indrec]);
		}
		else
		{
			walpha[indrec] = 0.0;
		}

		for (j = 0; j < potcrit->watoms; j++)
		{
			indj = potcrit->win[j];
			eij[0] = (at[i].x - at[indj].x) / rijrec[j]; 	
			eij[1] = (at[i].y - at[indj].y) / rijrec[j];
			eij[2] = (at[i].z - at[indj].z) / rijrec[j];

			tmp_units = ((at[i].vx*98.2266/sqrt(type[at[i].type].m)*eij[0] + at[i].vy*98.2266/sqrt(type[at[i].type].m)*eij[1] + at[i].vz*98.2266/sqrt(type[at[i].type].m)*eij[2])- (at[indj].vx*98.2266/sqrt(type[at[indj].type].m)*eij[0] + at[indj].vy*98.2266/sqrt(type[at[indj].type].m)*eij[1] + at[indj].vz*98.2266/sqrt(type[at[indj].type].m)*eij[2]));

			w[0][indrec] += (walpha[indrec] * rhorec[j] / rhobar[indrec]) * tmp_units * eij[0];
			w[1][indrec] += (walpha[indrec] * rhorec[j] / rhobar[indrec]) * tmp_units * eij[1];
			w[2][indrec] += (walpha[indrec] * rhorec[j] / rhobar[indrec]) * tmp_units * eij[2];
		}

		// Random forces for the recoil
		randrecvec[0] = gaussianrand();
		randrecvec[1] = gaussianrand();
		randrecvec[2] = gaussianrand();

		// Walpha and w's for others, calculate sigma at the same time
		for (ii = 0; ii < potcrit->watoms; ii++)
		{
			indi = potcrit->win[ii];

			currID = search(dictIndex, type[at[indi].type].Z);
			if (rhobar[ii] <= elstop->strs[currID][(elstop->ns[currID]) - 1])
			{
				walpha[ii] = splint(elstop->strs[currID], elstop->stopp[currID], elstop->stopp[currID], elstop->ns[currID], rhobar[ii]);
			}
			else
			{
				walpha[ii] = 0.0;
			}

			if (rijrec[ii] < potcrit->innerrad)	// [nlayer]
			{
				for (j = 0; j < nat; j++) 
				{

					if (rij[ii][j] > potcrit->wcut)
					{
						continue;
					}
					else if (indi != j)
					{

						eij[0] = (at[indi].x - at[j].x) / rij[ii][j]; // j is nat here and ii is watoms
						eij[1] = (at[indi].y - at[j].y) / rij[ii][j];
						eij[2] = (at[indi].z - at[j].z) / rij[ii][j];

						tmp_units = ((at[indi].vx*98.2266 / sqrt(type[at[indi].type].m)* eij[0] + at[indi].vy*98.2266 / sqrt(type[at[indi].type].m)* eij[1] + at[indi].vz*98.2266 / sqrt(type[at[indi].type].m) * eij[2])- (at[j].vx*98.2266 / sqrt(type[at[j].type].m)* eij[0] + at[j].vy*98.2266 / sqrt(type[at[j].type].m)* eij[1] + at[j].vz*98.2266 / sqrt(type[at[j].type].m)* eij[2]));

						w[0][ii] += (walpha[ii] * rho[ii][j] / rhobar[ii]) * tmp_units * eij[0];
						w[1][ii] += (walpha[ii] * rho[ii][j] / rhobar[ii]) * tmp_units * eij[1];
						w[2][ii] += (walpha[ii] * rho[ii][j] / rhobar[ii]) * tmp_units * eij[2];
					}
				}
			}

			else 	// Not inner atoms, calculating the effect of the recoil on them and the averaged components too.
			{
				indj = potcrit->win[ii];

				eij[0] = (at[indj].x - at[i].x) / rijrec[ii]; 	// with recoil
				eij[1] = (at[indj].y - at[i].y) / rijrec[ii];
				eij[2] = (at[indj].z - at[i].z) / rijrec[ii];

				tmp_units = ((at[indj].vx*98.2266 / sqrt(type[at[indj].type].m)* eij[0] + at[indj].vy*98.2266 / sqrt(type[at[indj].type].m)* eij[1] + at[indj].vz*98.2266 / sqrt(type[at[indj].type].m) * eij[2])- (at[i].vx*98.2266 / sqrt(type[at[i].type].m)* eij[0] + at[i].vy*98.2266 / sqrt(type[at[i].type].m)* eij[1] + at[i].vz*98.2266 / sqrt(type[at[i].type].m)* eij[2]));
						
				w[0][ii] = layer.avgw[0] + ((walpha[ii] * rho[ii][indrec] / rhobar[ii]) * eij[0]);
				w[1][ii] = layer.avgw[1] + ((walpha[ii] * rho[ii][indrec] / rhobar[ii]) * eij[1]);
				w[2][ii] = layer.avgw[2] + ((walpha[ii] * rho[ii][indrec] / rhobar[ii]) * eij[2]);
			}

			eta_factor = sqrt(2.0 * boltzman_const / time->dt[recoil->attype]);

			// indi is the W atom...
			eij[0] = (at[i].x - at[indi].x) / rijrec[ii];
			eij[1] = (at[i].y - at[indi].y) / rijrec[ii];
			eij[2] = (at[i].z - at[indi].z) / rijrec[ii];
			
			p1 = walpha[indrec] * rhorec[ii] * (w[0][indrec] * eij[0] + w[1][indrec] * eij[1] + w[2][indrec] * eij[2]); 	// effect of j on recoil
			p2 = walpha[ii] * rho[ii][i] * (w[0][ii] * eij[0] + w[1][ii] * eij[1] + w[2][ii] * eij[2]);

			r1 = 1 / rhobar[indrec];
			r2 = 1 / rhobar[ii];
			
			sigma[0] += eij[0] * ((r1 * p1) - (r2 * p2));
			sigma[1] += eij[1] * ((r1 * p1) - (r2 * p2));
			sigma[2] += eij[2] * ((r1 * p1) - (r2 * p2));
			
			randvec[0] = gaussianrand();
			randvec[1] = gaussianrand();
			randvec[2] = gaussianrand();

			q1 = walpha[indrec] * rhorec[ii] * (randrecvec[0] * eij[0] + randrecvec[1] * eij[1] + randrecvec[2] * eij[2]);
			q2 = walpha[ii] * rho[ii][i] * (randvec[0] * eij[0] + randvec[1] * eij[1] + randvec[2] * eij[2]);

			eta[0] += eij[0] * (r1 * q1 - r2 * q2);
			eta[1] += eij[1] * (r1 * q1 - r2 * q2);
			eta[2] += eij[2] * (r1 * q1 - r2 * q2);
		}

		eta_factor = (sqrtf(2.0 * boltzman_const / time->dt[recoil->attype]) * sqrtf(electronic_temp)); 	// Elec temp (nonlocal), timestep and boltzmann
		eta[0] *= eta_factor;
		eta[1] *= eta_factor;
		eta[2] *= eta_factor;

		wdeltav[0] = time->dt[recoil->attype] * (sigma[0] - eta[0]); 	
		wdeltav[1] = time->dt[recoil->attype] * (sigma[1] - eta[1]);
		wdeltav[2] = time->dt[recoil->attype] * (sigma[2] - eta[2]);

		Se = sigma[0] + sigma[1] + sigma[2];

		/* // GPK - output test
		if (file->printrhobarz)
		{
			printf("How many watoms %d - Atom j=1 rhobar walpha %.8f, %.8f\n", potcrit->watoms, rhobar[1], walpha[1]);
			printf("How many watoms %d - Atom j=2 rhobar walpha %.8f, %.8f\n", potcrit->watoms, rhobar[2], walpha[2]);
			printf("How many watoms %d - Atom j=3 rhobar walpha %.8f, %.8f\n", potcrit->watoms, rhobar[3], walpha[3]);
		}
		*/

		for (ii = 0; ii < potcrit->watoms + 1; ii++)
		{
			free(rij[ii]);
			free(rho[ii]);
		}
		free(rij);
		free(rho);
		free(rijrec);
		free(rhorec);
		free(w[0]);
		free(w[1]);
		free(w[2]);
		free(walpha);
		free(rhobar);
	} 	/* End, tensorial friction, AES GPK BS */

	if (file->printEz)
	{
		printf("%f, %f\n", recoil->sz, recoil->energy); /* Useful for getting E(z) */
	}

	if (debug)
		printf("S_e is %g \n", Se);

	if ((layer.elstop == 3))
	{ 	// GPK - Reduce velocity component-wise 
		at[i].vx -= wdeltav[0];
		at[i].vy -= wdeltav[1];
		at[i].vz -= wdeltav[2];
	}
	else
	{
		deltav = time->dt[recoil->attype] * Se; 	// GPK - uses Se if not using tensorial friction model
		temp = 1 - deltav / v;
		at[i].vx *= temp;
		at[i].vy *= temp;
		at[i].vz *= temp;
	}

	// GPK - for printing the histogram of closest distances / impact parameters. Here outside the getaccs loop over neighbouring atoms. 
	if (file->printdistlist)
	{
		if (potcrit->min_distance >= potcrit->min_hist && potcrit->min_distance <= potcrit->max_hist)
		{
			int bin_idx = (int)((potcrit->min_distance - potcrit->min_hist) / potcrit->bin_width);
			potcrit->histogram_singleion[bin_idx]++;
		}
	}

	if (file->printdens)
	{
		charge = 3.0 / (4.0 * PI * onelrad * onelrad * onelrad);
		k = (int)(charge / (10.0 / DEPENSIZE) + 0.5);
		/*printf("%d %i\n",k,k);*/
		if (k >= DEPENSIZE)
			k = DEPENSIZE;
		else if (k < 0)
			k = 0;
		elstop->dens[k] += 1;
		elstop->Sespc[k] += Se;
	}

	/*Firsov inelastic electronic stopping due to the change of electrons between nuclei*/
	if (elstop->Firsov)
	{
		if (type[recoil->attype].Z > 1)
		{
			/* We don't use the Firsov model for protons */
			avx = (at[i].vx * unit->vel[recoil->attype]);
			avy = (at[i].vy * unit->vel[recoil->attype]);
			avz = (at[i].vz * unit->vel[recoil->attype]);
			for (k = 0; k < potcrit->fatoms; k++) /* A loop over all colse neighbours */
			{
				/* For more info on the Firsov parametrization used, see Elteckov et al. in Atomic
					 Collision Phenomena in Solids, p. 657 */
				ain = potcrit->fin[k];
				atyp = at[ain].type;
				if (type[recoil->attype].Z > type[at[ain].type].Z)
				{
					z1 = type[recoil->attype].Z;
					z2 = type[at[ain].type].Z;
				}
				else
				{
					z2 = type[recoil->attype].Z;
					z1 = type[at[ain].type].Z;
				}
				alpha = 1 / (1 + pow((real)z2 / z1, 0.166666667));
				ACR = (1 + 0.8 * alpha * pow((real)z1, 0.33333333) * potcrit->frmin[k] / 0.46850229);
				ACR = pow(ACR, 4.0);
				BCR = (1 + 0.8 * (1 - alpha) * pow((real)z2, 0.33333333) * potcrit->frmin[k] / 0.46850229);
				BCR = pow(BCR, 4.0);
				SCR = (z1 * z1 / ACR + z2 * z2 / BCR) * 5.240844e-06; /*5.240844e-06=gfac*/
				fvx = avx - (at[potcrit->fin[k]].vx * unit->vel[at[potcrit->fin[k]].type]);
				fvy = avy - (at[potcrit->fin[k]].vy * unit->vel[at[potcrit->fin[k]].type]);
				fvz = avz - (at[potcrit->fin[k]].vz * unit->vel[at[potcrit->fin[k]].type]);
				firtotalx += SCR * fvx; /*force affecting to the ion by the neighbours*/
				firtotaly += SCR * fvy;
				firtotalz += SCR * fvz;
				/*in principle other atoms should feel  -F_firsov ..*/

				/*at[potcrit->fin[k]].vx+=SCR*fvx*time->dt[at[potcrit->fin[k]].type];*/
				/*at[potcrit->fin[k]].vy+=SCR*fvy*time->dt[at[potcrit->fin[k]].type];*/
				/*at[potcrit->fin[k]].vz+=SCR*fvz*time->dt[at[potcrit->fin[k]].type];*/

				/*firtotal+=sqrt(firtotalx*firtotalx+firtotaly*firtotaly+firtotalz*firtotalz);*/
			}

			firtotal = sqrt(firtotalx * firtotalx + firtotaly * firtotaly + firtotalz * firtotalz);
			/* printf("tot firsov %g\n",firtotal);*/
			elstop->totalfirsov = firtotal;
			/*firtotal*=time->dt[recoil->attype];   Force -> change in velocity, dv = (F/m) *dt
	at[i].vx*=1-(firtotal/v);
	at[i].vy*=1-(firtotal/v);
	at[i].vz*=1-(firtotal/v);*/
			at[i].vx -= firtotalx * time->dt[recoil->attype];
			at[i].vy -= firtotaly * time->dt[recoil->attype];
			at[i].vz -= firtotalz * time->dt[recoil->attype];
		}
	}
	else
		elstop->totalfirsov = 0;
	elstop->Se = Se;

	MAXDEBUGSRRR("elstop.c", elstop->v[nlayer][idown], elstop->v[nlayer][iup], Se);
}

/*
	 Calculate the Omega of the LSS model (Lindhard, Scharff, Schiott,
	 Mat. Fys. Medd. Dan. Vid. Selsk. 33, 14 (1963))
*/
real LSSOmega(
	int Z1,
	real Z2,
	real v,
	real N,
	real deltar,
	struct unit *unit,
	int t) /* t - Type of recoil atom */
{
	real x, omegaB, L;
	real Bohrvel = 2187700.0; /* SI units */
	real e = 1.602189e-19, eps0 = 8.854188e-12;
	real vSI;

	/* printf("%d %g %g %g %g\n",Z1,Z2,N,deltar,unit->length);  */
	/* N * deltar is in units of 1/A^2, i.e. correct for this to get SI */
	omegaB = (Z1 * e / (4 * PI * eps0) * sqrt(4 * PI * Z2 * N * deltar / (unit->length * unit->length)));

	vSI = v * unit->vel[t];
	x = vSI * vSI / (Bohrvel * Bohrvel * Z2);

	L = 1.0;
	if (x < 2.3)
	{
		L = 0.5 * (1.36 * sqrt(x) - 0.016 * sqrt(x * x * x));
	}

	MAXDEBUGSRRRR("LSSOmega", v, x, L, omegaB * sqrt(L));

	/* printf("%g %g %g %g\n",vSI,omegaB*sqrt(L),x,L); */

	return (omegaB * sqrt(L));
}

/* BS - Get average rhobar and w for EPH model */
int getAvgLatticeProps(
	struct layer *layer,
	struct atom *at,
	struct potcrit *potcrit,
	struct type *type,
	struct elstop *elstop,
	int nat)
{
	real walpha;
	real **r, **rho;
	real eij[3], vdiff[3];
	real minx, maxx, miny, maxy, minz, maxz;
	real *rhobar;
	real rhoSum = 0.0;
	int currID;
	real count = 0.0;
	real tmp;

	rhobar = (real *)calloc((nat), sizeof(real));
	r = (real **)malloc((nat) * sizeof(real *));
	for (int q = 0; q < nat; q++)
		r[q] = (real *)calloc((nat), sizeof(real));
	rho = (real **)malloc((nat) * sizeof(real *));
	for (int q = 0; q < nat; q++)
		rho[q] = (real *)calloc((nat), sizeof(real));

	/* Use the center of the lattice to avoid
	cuttofs from the edges of the coords file */
	minx = layer->latticeParam[0] * floor(((real)layer->Ncells[0]) / 2);
	miny = layer->latticeParam[1] * floor(((real)layer->Ncells[1]) / 2);
	minz = layer->latticeParam[2] * floor(((real)layer->Ncells[2]) / 2);
	maxx = minx + layer->latticeParam[0];
	maxy = miny + layer->latticeParam[1];
	maxz = minz + layer->latticeParam[2];

	layer->avgw[0] = 0.0;
	layer->avgw[1] = 0.0;
	layer->avgw[2] = 0.0;

	for (int q = 0; q < nat - 1; q++)
	{
		/* Only use atoms inside chosen volume */
		if ((at[q].x < minx) || (at[q].x > maxx))
			continue;
		if ((at[q].y < miny) || (at[q].y > maxy))
			continue;
		if ((at[q].z < minz) || (at[q].z > maxz))
			continue;

		/* Calculate average rhobar */
		for (int Q = 0; Q < nat - 1; Q++)
		{
			if (q == Q)
				continue;

			r[q][Q] = sqrt((at[Q].x - at[q].x) * (at[Q].x - at[q].x) + (at[Q].y - at[q].y) * (at[Q].y - at[q].y) + (at[Q].z - at[q].z) * (at[Q].z - at[q].z));
			if (r[q][Q] < potcrit->wcut)
			{
				currID = search(dictIndex, type[at[Q].type].Z);
				rho[q][Q] = splint(elstop->cdist[currID], elstop->celdens[currID], elstop->celdens[currID], elstop->nc[currID], r[q][Q]);
				rhobar[q] = rhobar[q] + rho[q][Q];
			}
		}
		rhoSum = rhoSum + rhobar[q];

		/* Calculate average w */
		currID = search(dictIndex, type[at[q].type].Z);
		walpha = splint(elstop->strs[currID], elstop->stopp[currID], elstop->stopp[currID], elstop->ns[currID], rhobar[q]);
		for (int Q = 0; Q < nat - 1; Q++)
		{
			if ((rho[q][Q] == 0.0) || (q == Q))
				continue;

			eij[0] = (at[q].x - at[Q].x) / r[q][Q];
			eij[1] = (at[q].y - at[Q].y) / r[q][Q];
			eij[2] = (at[q].z - at[Q].z) / r[q][Q];

			tmp = walpha * rho[q][Q] / rhobar[q] * ((at[q].vx*98.2266 / sqrt(type[at[q].type].m)* eij[0] + at[q].vy*98.2266 / sqrt(type[at[q].type].m)* eij[1] + at[q].vz*98.2266 / sqrt(type[at[q].type].m) * eij[2]) - (at[Q].vx*98.2266 / sqrt(type[at[Q].type].m)* eij[0] + at[Q].vy*98.2266 / sqrt(type[at[Q].type].m)* eij[1] + at[Q].vz*98.2266 / sqrt(type[at[Q].type].m)* eij[2]));

			layer->avgw[0] = layer->avgw[0] + (tmp * eij[0]);
			layer->avgw[1] = layer->avgw[1] + (tmp * eij[1]);
			layer->avgw[2] = layer->avgw[2] + (tmp * eij[2]);
		}
		count = count + 1.0;
	}
	layer->avgRhobar = rhoSum / count;

	layer->avgw[0] = layer->avgw[0] / count;
	layer->avgw[1] = layer->avgw[1] / count;
	layer->avgw[2] = layer->avgw[2] / count;

	/* Free memory */
	for (int q = 0; q < nat; q++)
	{
		free(r[q]);
		free(rho[q]);
	}
	free(r);
	free(rho);
	free(rhobar);
}

/* BS - Get default inner radius value - currently not used as there is just 1 EPH layer */
void setEPHInnerRadius(
	struct layers *layers,
	struct potcrit *potcrit)
{
	/* Set innerradius (after which the averaging approximation is used) */
	for (int j = 0; j < layers->Nlayers; j++) /*layer loop*/
	{
		/*
		if (potcrit->innerrad[j] < 0.0)
		{
			if (layers->layer[j].latticeParam[2] > potcrit->wcut)
				potcrit->innerrad[j] = potcrit->wcut;
			else
				potcrit->innerrad[j] = layers->layer[j].latticeParam[2];
		}
		*/
	}
}
